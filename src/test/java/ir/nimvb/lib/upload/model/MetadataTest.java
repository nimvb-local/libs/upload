package ir.nimvb.lib.upload.model;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.AbstractMap;
import java.util.Map;

class MetadataTest {

    @Test
    void givenTheStringWithNullValue_shouldReturnEmptyString() {
        String encode = Metadata.Encoder.encode((String) null);
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isEmpty();
    }

    @Test
    void givenTheEmptyString_shouldReturnEmptyString() {
        String encode = Metadata.Encoder.encode("");
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isEmpty();
    }

    @Test
    void givenTheBlankString_shouldNotReturnEmptyString() {
        String encode = Metadata.Encoder.encode(" ");
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isNotEmpty();
        Assertions.assertThat(encode).isNotBlank();
    }

    @Test
    void givenTheKeyWithNullValue_AndTheNullValue_shouldReturnEmptyString() {
        Map.Entry<String,String> entry = new AbstractMap.SimpleEntry<>(null,null);
        String encode = Metadata.Encoder.encode(entry);
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isEmpty();
    }

    @Test
    void givenTheKeyWithNullValue_AndTheEmptyValue_shouldReturnEmptyString() {
        Map.Entry<String,String> entry = new AbstractMap.SimpleEntry<>(null,"");
        String encode = Metadata.Encoder.encode(entry);
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isEmpty();
    }

    @Test
    void givenTheKeyWithNullValue_AndTheBlankValue_shouldReturnEmptyString() {
        Map.Entry<String,String> entry = new AbstractMap.SimpleEntry<>(null," ");
        String encode = Metadata.Encoder.encode(entry);
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isEmpty();
    }

    @Test
    void givenTheKeyWithEmptyValue_AndTheNullValue_shouldReturnEmptyString() {
        Map.Entry<String,String> entry = new AbstractMap.SimpleEntry<>("",null);
        String encode = Metadata.Encoder.encode(entry);
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isEmpty();
    }

    @Test
    void givenTheKeyWithEmptyValue_AndTheEmptyValue_shouldReturnEmptyString() {
        Map.Entry<String,String> entry = new AbstractMap.SimpleEntry<>("","");
        String encode = Metadata.Encoder.encode(entry);
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isEmpty();
    }

    @Test
    void givenTheKeyWithBlankValue_AndTheNullValue_shouldNotReturnEmptyString() {
        Map.Entry<String,String> entry = new AbstractMap.SimpleEntry<>(" ",null);
        String encode = Metadata.Encoder.encode(entry);
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isNotEmpty();
        Assertions.assertThat(encode.endsWith(Metadata.NULL)).isTrue();
    }
    @Test
    void givenTheKeyWithBlankValue_AndTheEmptyValue_shouldNotReturnEmptyString() {
        Map.Entry<String,String> entry = new AbstractMap.SimpleEntry<>(" ","");
        String encode = Metadata.Encoder.encode(entry);
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isNotEmpty();
    }

    @Test
    void givenTheKeyWithBlankValue_AndTheBlankValue_shouldNotReturnEmptyString() {
        Map.Entry<String,String> entry = new AbstractMap.SimpleEntry<>(" "," ");
        String encode = Metadata.Encoder.encode(entry);
        Assertions.assertThat(encode).isNotNull();
        Assertions.assertThat(encode).isNotEmpty();
    }


    @Test
    void givenTheStringWithNullValue_shouldReturnNull() {
        String encoded = null;
        Map.Entry<String, String> decode = Metadata.Decoder.decodeAsEntry(encoded);
        Assertions.assertThat(decode).isNull();
    }

    @Test
    void givenTheStringWithBlankValue_shouldReturnNull() {
        String encoded = " ";
        Map.Entry<String, String> decode = Metadata.Decoder.decodeAsEntry(encoded);
        Assertions.assertThat(decode).isNull();
    }

    @Test
    void givenTheStringWithSingleEncodedValue_shouldReturnEntryWithKeyAndEmptyValue() {
        String encoded = "AI==";
        Map.Entry<String, String> decode = Metadata.Decoder.decodeAsEntry(encoded);
        Assertions.assertThat(decode).isNotNull();
        Assertions.assertThat(decode.getKey()).isNotBlank();
        Assertions.assertThat(decode.getValue()).isEmpty();
    }

    @Test
    void givenTheStringWithEncodedValues_ThatContainsNullValue_shouldReturnEntryWithKeyAndNullValue() {
        String encoded = Metadata.Encoder.encode(" ",null);
        Map.Entry<String, String> decode = Metadata.Decoder.decodeAsEntry(encoded);
        Assertions.assertThat(decode).isNotNull();
        Assertions.assertThat(decode.getKey()).isNotEmpty();
        Assertions.assertThat(decode.getValue()).isNull();
    }

    @Test
    void givenTheStringWithEncodedValues_ThatContainsEncodedValue_shouldReturnEntryWithKeyAndValue() {
        String encoded = "AI== " + Metadata.NULL;
        Map.Entry<String, String> decode = Metadata.Decoder.decodeAsEntry(encoded);
        Assertions.assertThat(decode).isNotNull();
        Assertions.assertThat(decode.getKey()).isNotBlank();
        Assertions.assertThat(decode.getValue()).isNull();
    }


    @Test
    void givenTheStringWithInvalidEncodedValues_shouldReturnEntryWithNullValue() {
        String encoded = "AI=== ";
        Map.Entry<String, String> decode = Metadata.Decoder.decodeAsEntry(encoded);
        Assertions.assertThat(decode).isNull();
    }

    @Test
    void givenTheStringWithEncodedValues_shouldReturnMetadata() {
        Metadata metadata = new Metadata();
        metadata
                .add(" ","")
                .add("1"," ");
        Metadata decode = Metadata.Decoder.decode(metadata.encode());
        Assertions.assertThat(decode).isNotNull();
    }

}