package ir.nimvb.lib.upload.controller.step.dispatch.context;

import com.nimvb.lib.pattern.behavioral.chain.Chain;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.builder.ExecutorBuilder;
import ir.nimvb.lib.upload.controller.manager.dispatch.context.RequestHandlersConstructionStep;
import ir.nimvb.lib.upload.controller.manager.dispatch.context.RequestHandlersMatchingStep;
import ir.nimvb.lib.upload.controller.manager.dispatch.context.RequestHandlersScanningStep;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.StepsBuilder;

public class RequestDispatchStepsBuilder implements StepsBuilder {

    public static final RequestDispatchStepsBuilder INSTANCE = new RequestDispatchStepsBuilder();


    @Override
    public ChainExecutor build(DependencyManager dependencyManager) {
        ExecutorBuilder builder = Chain.INSTANCE.builder();
        builder.add(new RequestHandlersScanningStep(dependencyManager));
        builder.add(new RequestHandlersConstructionStep(dependencyManager));
        builder.add(new RequestHandlersMatchingStep(dependencyManager));
        return builder.build();
    }
}
