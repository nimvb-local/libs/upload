package ir.nimvb.lib.upload.controller.manager.dispatch.context;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.controller.model.DispatcherContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManager;
import ir.nimvb.lib.upload.service.common.request.RequestHandler;
import org.springframework.http.HttpMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class RequestHandlersMatchingStep extends AbstractStepManager {
    public RequestHandlersMatchingStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(request, response, dependencyManager);
    }

    public RequestHandlersMatchingStep(DependencyManager dependencyManager) {
        this(null, null, dependencyManager);
    }

    @Override
    protected <T> Class<T> contentType() {
        return (Class<T>) DispatcherContext.class;
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        DispatcherContext dispatcherContext = (DispatcherContext) context.getContent();
        Collection<RequestHandler> handlers = dispatcherContext.getHandlers();
        dispatcherContext.setHandlersOfTheMethod(match(handlers));
        chain.next(context);
        return context;
    }

    Map<HttpMethod, Collection<RequestHandler>> match(Collection<RequestHandler> handlers) {
        return matchMethod(handlers);
    }

    Map<HttpMethod, Collection<RequestHandler>> matchMethod(Collection<RequestHandler> handlers) {
        Map<HttpMethod, Collection<RequestHandler>> candidates = new HashMap<>();
        HttpMethod method;
        for (RequestHandler handler : handlers) {


            method = handler.method();
            if (candidates.containsKey(method)) {
                candidates.get(method).add(handler);
            } else {
                candidates.put(method, new ArrayList<>(Collections.singletonList(handler)));
            }

        }
        return candidates;
    }

//    Collection<RequestHandler> matchPath(Collection<RequestHandler> handlers) {
//        PathMatcher matcher = dependencyManager.getPathMatcher();
//        String requestURI = request.getRequestURI();
//        return handlers.stream()
//                .filter(requestHandler -> {
//                    String pattern = requestHandler.path();
//                    return matcher.match(pattern, requestURI);
//                })
//                .collect(Collectors.toList());
//    }


}
