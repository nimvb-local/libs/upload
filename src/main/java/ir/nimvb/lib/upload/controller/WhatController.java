package ir.nimvb.lib.upload.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.io.OutputStream;

@RestController
@RequestMapping("/api/")
public class WhatController {
    @GetMapping("/yes/{p}")
    String yes(@PathVariable(required = false) String p) {
        return "pes";
    }

    @GetMapping("/no")
    ResponseEntity<StreamingResponseBody> mAsync() {
        return ResponseEntity.ok(new StreamingResponseBody() {
            @Override
            public void writeTo(OutputStream outputStream) throws IOException {
                try {
                    Thread.sleep(1000 * 5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
