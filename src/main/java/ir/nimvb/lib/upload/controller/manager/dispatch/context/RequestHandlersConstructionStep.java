package ir.nimvb.lib.upload.controller.manager.dispatch.context;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.controller.model.DispatcherContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.request.NotFoundRequestHandler;
import ir.nimvb.lib.upload.service.common.request.RequestHandler;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManager;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RequestHandlersConstructionStep extends AbstractStepManager {
    public RequestHandlersConstructionStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(request, response, dependencyManager);
    }

    public RequestHandlersConstructionStep(DependencyManager dependencyManager) {
        this(null, null, dependencyManager);
    }

    @Override
    protected <T> Class<T> contentType() {
        return (Class<T>) DispatcherContext.class;
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        DispatcherContext dispatcherContext = (DispatcherContext) context.getContent();
        BeanDefinitionRegistry registry = dispatcherContext.getRegistry();
        String[] beans = registry.getBeanDefinitionNames();
        List<RequestHandler> handlers = Arrays.stream(beans)
                .map(name -> {
                    Optional<? extends Class<?>> clazz = Optional.empty();
                    try {

                        clazz = Optional.ofNullable(Class.forName(registry.getBeanDefinition(name).getBeanClassName()));
                    } catch (ClassNotFoundException e) {

                    }
                    return clazz;
                })
                .filter(clazz -> clazz.isPresent())
                .map(clazz -> clazz.get())
                .map(clazz -> {
                    Optional<?> instance = Optional.empty();
                    try {
                        instance = Optional
                                .ofNullable
                                        (
                                                clazz
                                                        .getConstructor(DependencyManager.class)
                                                        .newInstance(dependencyManager)
                                        );
                    } catch (Exception ignored) {

                    }
                    return instance;
                })
                .filter(instance -> instance.isPresent())
                .map(instance -> ((RequestHandler) instance.get()))
                .collect(Collectors.toList());
        dispatcherContext.setHandlers(handlers);
        dispatcherContext.setNotFoundHandler(new NotFoundRequestHandler(dependencyManager));
        chain.next(context);
        return context;
    }
}
