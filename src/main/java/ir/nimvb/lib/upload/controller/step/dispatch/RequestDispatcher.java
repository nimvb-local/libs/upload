package ir.nimvb.lib.upload.controller.step.dispatch;

import ir.nimvb.lib.upload.controller.model.DispatcherContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.request.RequestHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.PathMatcher;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class RequestDispatcher {
    private final DispatcherContext context;
    private final DependencyManager dependencyManager;


    public void dispatch(HttpServletRequest request, HttpServletResponse response) {
        RequestHandler handler = find(HttpMethod.valueOf(request.getMethod()), request);
        if (handler != null) {
            handler.handler().execute(request, response);
        }
    }

    RequestHandler find(HttpMethod method, HttpServletRequest request) {
        if (context.getHandlersOfTheMethod().containsKey(method)) {
            Collection<RequestHandler> handlers = context.getHandlersOfTheMethod().get(method);
            Optional<RequestHandler> handler = matchPath(request, handlers).stream().findFirst();
            if (handler.isPresent()) {
                Map<String, String> variables = new HashMap<>();
                try {
                    variables = dependencyManager.getPathMatcher().extractUriTemplateVariables(baseUrl() + handler.get().path(), request.getRequestURI());
                    variables = dependencyManager.getUrlPathHelper().decodePathVariables(request,variables);
                }catch (Exception ignored){

                }
                request.setAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE,variables);
                return handler.get();
            }
        }

        return context.getNotFoundHandler();
    }

    Collection<RequestHandler> matchPath(HttpServletRequest request, Collection<RequestHandler> handlers) {
        PathMatcher matcher = dependencyManager.getPathMatcher();
        return handlers.stream()
                .filter(requestHandler -> {
                    String pattern = baseUrl() + requestHandler.path();
                    return matcher.match(pattern, request.getRequestURI());
                })
                .collect(Collectors.toList());
    }

    String baseUrl(){
        String urlPattern = dependencyManager.getProperties().getServlet().getUrlPattern();
        if(!urlPattern.startsWith("/")){
            urlPattern = "/" + urlPattern;
        }
        if(urlPattern.endsWith("/*")){
            urlPattern = urlPattern.substring(0,urlPattern.lastIndexOf("/*"));
        }
        return urlPattern;
    }


}
