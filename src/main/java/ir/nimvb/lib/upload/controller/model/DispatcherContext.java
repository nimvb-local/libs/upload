package ir.nimvb.lib.upload.controller.model;

import ir.nimvb.lib.upload.service.common.request.RequestHandler;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public class DispatcherContext {
    @Getter
    BeanDefinitionRegistry registry = new SimpleBeanDefinitionRegistry();
    @Getter
    @Setter
    Collection<RequestHandler> handlers = new ArrayList<>();
    @Getter
    @Setter
    RequestHandler notFoundHandler = null;
    @Getter
    @Setter
    Map<HttpMethod, Collection<RequestHandler>> handlersOfTheMethod = new HashMap<>();

}
