package ir.nimvb.lib.upload.controller.manager.dispatch.context;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.controller.model.DispatcherContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.request.AbstractRequestHandler;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManager;
import ir.nimvb.lib.upload.service.common.request.RequestHandler;
import ir.nimvb.lib.upload.service.download.DownloadRequestHandler;
import ir.nimvb.lib.upload.service.protocol.AbstractTusProtocolRequestHandler;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.filter.AssignableTypeFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestHandlersScanningStep extends AbstractStepManager {

    public RequestHandlersScanningStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(request, response, dependencyManager);
    }

    public RequestHandlersScanningStep(DependencyManager dependencyManager){
        this(null,null,dependencyManager);
    }

    @Override
    protected <T> Class<T> contentType() {
        return (Class<T>) DispatcherContext.class;
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        DispatcherContext dispatcherContext = (DispatcherContext) context.getContent();
        ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(dispatcherContext.getRegistry());
        scanner.resetFilters(false);
        scanner.setIncludeAnnotationConfig(false);
        scanner.addIncludeFilter(new AssignableTypeFilter(RequestHandler.class));
        scanner.scan(AbstractRequestHandler.class.getPackageName());
        scanner.scan(AbstractTusProtocolRequestHandler.class.getPackageName());
        scanner.scan(DownloadRequestHandler.class.getPackageName());
        chain.next(context);
        return context;
    }
}
