package ir.nimvb.lib.upload.controller;


import ir.nimvb.lib.upload.controller.step.dispatch.RequestDispatcher;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class ManagementServlet extends HttpServlet {

    @Autowired
    private DependencyManager factory;

    @Autowired
    private RequestDispatcher dispatcher;

//    @Autowired
//    private ObjectManager manager;
//
//    @Autowired
//    @Lazy
//    private PathMatcher pathMatcher;
//
//    @Autowired
//    @Lazy
//    private UrlPathHelper urlPathHelper;



    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        dispatcher.dispatch(req,resp);
    }
}
