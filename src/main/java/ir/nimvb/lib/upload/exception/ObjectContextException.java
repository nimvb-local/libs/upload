package ir.nimvb.lib.upload.exception;

public class ObjectContextException extends RuntimeException{
    public ObjectContextException(Throwable e){
        super(e);
    }
}
