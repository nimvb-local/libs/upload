package ir.nimvb.lib.upload.config;

import com.amazonaws.services.s3.AmazonS3;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
import ir.nimvb.lib.upload.service.manager.ObjectManager;
import ir.nimvb.lib.upload.service.store.DataStore;
import ir.nimvb.lib.upload.service.store.context.ObjectContextStore;
import ir.nimvb.lib.upload.service.store.context.s3.S3Store;
import ir.nimvb.lib.upload.service.store.s3.S3DataStore;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;

@RequiredArgsConstructor
@Configuration
public class UploadLibraryConfiguration {
    private final TusUploadConfigurationProperties uploadConfigurationProperties;
    private final AmazonS3 s3Client;

    @Bean
    public ObjectMapper mapper() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        return objectMapper;
    }

    @Bean
    public DataStore dataStore(TusUploadConfigurationProperties properties) {
        return new S3DataStore(s3Client,"nimvb");
        //return new FileDataStore(properties);
    }

    @Bean
    public DataStore.Writer writer(DataStore dataStore){
        //return ((FileDataStore) dataStore);
        return ((S3DataStore) dataStore);
    }

    @Bean
    public DataStore.Reader reader(DataStore dataStore){
        //return ((FileDataStore) dataStore);
        return new DataStore.Reader() {
            @Override
            public byte[] read(String id, long offset, long length) {
                return new byte[0];
            }

            @Override
            public InputStream getInputStream(String id) throws IOException {
                return null;
            }
        };
    }

    @Bean
    public ObjectContextStore objectContextStore(TusUploadConfigurationProperties properties, ObjectMapper mapper) {
        return new S3Store(s3Client,"nimvb",mapper);
        //return new FileStore(properties,mapper);
    }


    @Bean
    public ObjectManager objectManager(DataStore store,DataStore.Writer writer,DataStore.Reader reader,ObjectContextStore objectContextStore) {
        return new ObjectManager(store,writer,reader,objectContextStore);
    }
}
