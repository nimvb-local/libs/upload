package ir.nimvb.lib.upload.config;


import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.controller.model.DispatcherContext;
import ir.nimvb.lib.upload.controller.step.dispatch.context.RequestDispatchStepsBuilder;
import ir.nimvb.lib.upload.filter.TusFilter;
import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
import ir.nimvb.lib.upload.controller.ManagementServlet;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.util.Collections;

@RequiredArgsConstructor
@Configuration
public class UploadLibraryServletConfiguration {

    private final TusUploadConfigurationProperties uploadConfigurationProperties;
    private final AutowireCapableBeanFactory beanFactory;
    //private final DummyController controller;
    //private final SimpleUrlHandlerMapping simpleUrlHandlerMapping;

//    @Primary
//    @Bean
//    public SimpleUrlHandlerMapping mmm() {
//        SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
//        HashMap<String, Object> objectObjectHashMap = new HashMap<>();
//        objectObjectHashMap.put("/yes/**",controller);
//        mapping.setUrlMap(objectObjectHashMap);
//        mapping.setOrder(0);
//        return mapping;
//    }

    @Primary
    @Bean
    @ConditionalOnMissingBean
    public AsyncTaskExecutor asyncTaskExecutor(){
        return new SimpleAsyncTaskExecutor("tus");
    }

    @Bean
    public ServletRegistrationBean<ManagementServlet> dummyServletRegistration(ManagementServlet servlet) {
        ServletRegistrationBean<ManagementServlet> bean = new ServletRegistrationBean<>(
                servlet, uploadConfigurationProperties.getServlet().getUrlPattern());
        bean.setLoadOnStartup(1);
        return bean;
    }

    @Bean
    public FilterRegistrationBean<TusFilter> dummyFilterRegistration() {
        FilterRegistrationBean<TusFilter> bean = new FilterRegistrationBean<>();
        TusFilter filter = new TusFilter();
        beanFactory.autowireBean(filter);
        bean.setFilter(filter);
        bean.setUrlPatterns(Collections.singleton(uploadConfigurationProperties.getServlet().getUrlPattern()));
        return bean;
    }

    @Bean
    DispatcherContext dispatcherContext(DependencyManager dependencyManager){
        StageContext context = new StageContext();
        context.setContent(new DispatcherContext());
        return (DispatcherContext) RequestDispatchStepsBuilder.INSTANCE.build(dependencyManager).execute(context).content();
    }
}
