package ir.nimvb.lib.upload.filter.http;

import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.util.Arrays;

public class CorsResponseWrapper extends HttpServletResponseWrapper {


    /**
     * Constructs a response adaptor wrapping the given response.
     *
     * @param response   The response to be wrapped
     * @param properties
     * @throws IllegalArgumentException if the response is null
     */
    public CorsResponseWrapper(HttpServletResponse response, TusUploadConfigurationProperties properties) {
        super(response);
        if (!properties.getCors().isSkip()) {
            this.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, properties.getCors().getOrigins());
            this.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, Arrays.stream(properties.getCors().getAcceptableMethods()).reduce((s, s2) -> String.format("%s, %s", s, s2)).orElse(""));
            this.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, Arrays.stream(properties.getCors().getAcceptableHeaders()).reduce((s, s2) -> String.format("%s, %s", s, s2)).orElse(""));
            this.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, Arrays.stream(properties.getCors().getExposedHeaders()).reduce((s, s2) -> String.format("%s, %s", s, s2)).orElse(""));
            this.addHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, String.valueOf(properties.getCors().isAllowCredentials()));
            this.addIntHeader(HttpHeaders.ACCESS_CONTROL_MAX_AGE, (properties.getCors().getMaxAge()));
        }
    }


}
