package ir.nimvb.lib.upload.filter.http;

import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class TusRequestWrapper extends HttpServletRequestWrapper {


    /**
     * Constructs a request object wrapping the given request.
     *
     * @param request The request to wrap
     * @throws IllegalArgumentException if the request is null
     */
    public TusRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getMethod() {
        String value = getHeader(TusHttpHeaders.X_HTTP_METHOD_OVERRIDE);
        if(value != null && !value.isBlank()){
            return value;
        }
        return super.getMethod();
    }
}
