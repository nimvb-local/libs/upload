package ir.nimvb.lib.upload.filter;

import ir.nimvb.lib.upload.filter.http.TusRequestWrapper;
import ir.nimvb.lib.upload.filter.http.CorsResponseWrapper;
import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TusFilter extends OncePerRequestFilter {

    @Autowired
    private TusUploadConfigurationProperties properties;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        TusRequestWrapper wrapper = new TusRequestWrapper(request);
        CorsResponseWrapper responseWrapper = new CorsResponseWrapper(response,properties);
        filterChain.doFilter(wrapper,responseWrapper);
    }
}
