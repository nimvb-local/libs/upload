package ir.nimvb.lib.upload.property;

import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Data
@Configuration
@ConfigurationProperties(prefix = "spring.tuts.upload")
public class TusUploadConfigurationProperties {
    @Data
    public static class Storage {
        /**
         * Maximum storage space to use
         */
        private Size size = new Size();
    }

    @Data
    public static class Size {
        /**
         * Maximum size of an object or operation
         */
        private long maximum = 0L;
    }

    @Data
    public static class Request {
        /**
         * Maximum number of bytes acceptable in patch request
         */
        private Size size = new Size();
    }

    @Data
    public static class Servlet {
        private String urlPattern = "/uploads/*";
    }

    @Data
    public static class Cors {
        private String origins = "*";
        private String[] acceptableMethods = Arrays.asList("GET", "POST", "DELETE", "PUT", "PATCH", "HEAD").toArray(new String[0]);
        private String[] acceptableHeaders =
                Stream.concat
                        (
                                Arrays.asList
                                        (
                                                "Origin",
                                                "x-request-id",
                                                "Accept",
                                                "X-Requested-With",
                                                "Content-Type",
                                                "Access-Control-Request-Method",
                                                "Access-Control-Request-Headers"
                                        ).stream(),
                                Arrays.stream(TusHttpHeaders.HEADERS)
                        )
                        .collect(Collectors.toList())
                        .toArray(new String[0]);
        private String[] exposedHeaders =
                Stream.concat
                        (
                                Stream.of
                                        (
                                                "Cache-control",
                                                "Location",
                                                "Access-Control-Allow-Origin",
                                                "Access-Control-Allow-Credentials"
                                        ),
                                Arrays.stream(TusHttpHeaders.HEADERS)

                        )
                        .collect(Collectors.toList())
                        .toArray(new String[0]);
        private boolean allowCredentials = true;
        public int maxAge = 10;
        private boolean skip = false;

    }

    private String folder = "/tmp";
    private Request request = new Request();
    private Storage storage = new Storage();
    private Servlet servlet = new Servlet();
    private Cors cors = new Cors();


}
