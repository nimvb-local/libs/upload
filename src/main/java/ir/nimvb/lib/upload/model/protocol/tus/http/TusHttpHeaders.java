package ir.nimvb.lib.upload.model.protocol.tus.http;

import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class TusHttpHeaders {
    public static final String X_HTTP_METHOD_OVERRIDE = "X-HTTP-Method-Override";
    public static final String UPLOAD_OFFSET = "Upload-Offset";
    public static final String UPLOAD_LENGTH = "Upload-Length";
    public static final String UPLOAD_METADATA = "Upload-Metadata";
    public static final String UPLOAD_DEFER_LENGTH = "Upload-Defer-Length";
    public static final String TUS_VERSION = "Tus-Version";
    public static final String TUS_RESUMABLE = "Tus-Resumable";
    public static final String TUS_EXTENSION = "Tus-Extension";
    public static final String TUS_MAX_SIZE = "Tus-Max-Size";
    public static String[] HEADERS = headers();

    private static String[] headers() {
        List<String> headers = new ArrayList<>();
        ReflectionUtils.doWithFields(TusHttpHeaders.class, new ReflectionUtils.FieldCallback() {
                    @Override
                    public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                        Object value = field.get(null);
                        headers.add(String.valueOf(value));
                    }
                }
                , field -> {
                    boolean isStatic = Modifier.isStatic(field.getModifiers());
                    boolean isFinal = Modifier.isFinal(field.getModifiers());
                    return isStatic && isFinal;
                });
        return headers.toArray(new String[0]);
    }

}
