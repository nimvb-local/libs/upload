package ir.nimvb.lib.upload.model.protocol.tus.http;

import org.springframework.http.MediaType;

public class TusMediaType extends MediaType {
    public static final TusMediaType APPLICATION_OFFSET_OCTET_STREAM = new TusMediaType("application", "offset+octet-stream");
    public static final String APPLICATION_OFFSET_OCTET_STREAM_VALUE = "application/offset+octet-stream";

    private TusMediaType(String type, String subtype) {
        super(type, subtype);
    }
}
