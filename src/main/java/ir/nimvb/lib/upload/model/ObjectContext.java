package ir.nimvb.lib.upload.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.util.Assert;

import java.util.Optional;
import java.util.UUID;

@Data
public class ObjectContext implements Cloneable {
    private long offset = -1;
    private long length = -1;
    private String id = UUID.randomUUID().toString().replace("-","");;
    private Metadata metadata = new Metadata();

    public static final class MetadataAttributes {
        public static final String FILENAME = "filename";
        public static final String USERNAME = "username";
        public static final String CHECKSUM = "checksum";
        public static final String UPLOAD_LENGTH = "upload-length";
    }

    public void setUploadLength(String length){
        this.length = Long.parseLong(length);
        //metadata.add(MetadataAttributes.UPLOAD_LENGTH,length);
    }

    @JsonIgnore
    public Optional<String> getUploadLength() {
        return Optional.of(String.valueOf(this.length));
        //return metadata.value(MetadataAttributes.UPLOAD_LENGTH);
    }

    @JsonIgnore
    public void setFilename(String filename){
        metadata.add(MetadataAttributes.FILENAME,filename);
    }

    @JsonIgnore
    public Optional<String> getFilename() {
        return metadata.value(MetadataAttributes.FILENAME);
    }


    public ObjectContext(long offset,long length) {
        this.offset = offset;
        this.length = length;
        this.metadata = new Metadata();
    }

    public ObjectContext(long offset,long length,String metadata) {
        this.offset = offset;
        this.length = length;
        this.metadata = Metadata.Decoder.decode(metadata);
    }

    public ObjectContext(ObjectContext other){
        Assert.notNull(other, "other is null");
        this.id = other.id;
        this.offset = other.offset;
        this.length = other.length;
        this.metadata = Metadata.Decoder.decode(other.getMetadata().encode());

    }

    public ObjectContext() {}

    @Override
    public ObjectContext clone() throws CloneNotSupportedException {
        ObjectContext context = (ObjectContext) super.clone();
        context.metadata = Metadata.Decoder.decode(Metadata.Encoder.encode(context.metadata));
        return context;
    }
}
