package ir.nimvb.lib.upload.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ir.nimvb.lib.upload.service.converter.deserializer.MetadataDeserializer;
import ir.nimvb.lib.upload.service.converter.serializer.MetadataSerializer;

import java.nio.charset.StandardCharsets;
import java.util.*;

@JsonSerialize(using = MetadataSerializer.class)
@JsonDeserialize(using = MetadataDeserializer.class)
public class Metadata {
    private Map<String, String> map = new HashMap<>();
    public static final String NULL = "%%NULL%%";

    public int size() {
        return map.size();
    }

    public Metadata add(String key, String value) {
        if (key == null || value == null) {
            return this;
        }
        this.map.put(key, value);
        return this;
    }

    public boolean exists(String key) {
        if (key == null) {
            return false;
        }
        return this.map.containsKey(key);
    }

    public Optional<String> value(String key) {
        if (exists(key)) {
            return Optional.ofNullable(this.map.get(key));
        }
        return Optional.empty();
    }

    public Metadata delete(String key) {
        if (key == null) {
            this.map.remove(key);
        }
        return this;
    }

    public String encode() {
        return Encoder.encode(this);
    }


    public static class Encoder {

        public static String encode(Metadata metadata) {
            Map<String, String> map = metadata.map;
            if (map == null) {
                return "";
            }
            List<String> encodedPairs = new ArrayList<>();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String encodedEntry = encode(entry);
                if (!encodedEntry.isEmpty()) {
                    encodedPairs.add(encodedEntry);
                }
            }
            return encodedPairs.stream().reduce((s, s2) -> String.format("%s,%s", s, s2)).orElse("");
        }

        public static String encode(Map.Entry<String, String> entry) {
            return encode(entry.getKey(), entry.getValue());
        }

        public static String encode(String key, String value) {
            if (key == null || key.isEmpty()) {
                return "";
            }
            String encodedKey = encodeKey(key);
            String encodedValue = encode(value);
            if (value == null) {
                encodedValue = NULL;
            }
            return String.format("%s %s", encodedKey, encodedValue).stripLeading().stripTrailing();
        }

        public static String encode(String value) {
            if (value == null) {
                return "";
            }
            return Base64.getUrlEncoder().encodeToString(value.getBytes(StandardCharsets.UTF_8));
        }

        public static String encodeKey(String key){
            if (key == null) {
                return "";
            }
            return key;
        }
    }

    public static class Decoder {


        public static Metadata decode(String encodedMetadata) {
            if(encodedMetadata == null || encodedMetadata.isEmpty() || encodedMetadata.isBlank()){
                return null;
            }
            String[] parts = encodedMetadata.split(",");
            Metadata metadata = new Metadata();
            for (String part : parts) {
                Map.Entry<String, String> entry = decodeAsEntry(part);
                metadata.add(entry.getKey(),entry.getValue());
            }
            return metadata;
        }

        public static Map.Entry<String, String> decodeAsEntry(String encodedValue) {
            if (encodedValue == null || encodedValue.strip().isEmpty()) {
                return null;
            }
            encodedValue = encodedValue.strip();
            String[] parts = encodedValue.split(" ");
            String key = "";
            String value = "";

            try {
                key = parts[0];//new String(Base64.getUrlDecoder().decode(parts[0].getBytes(StandardCharsets.UTF_8)));
                if (parts.length == 2) {
                    if (parts[1].equals(NULL)) {
                        value = null;
                    } else {
                        value = new String(Base64.getUrlDecoder().decode(parts[1].getBytes(StandardCharsets.UTF_8)));
                    }
                }
            } catch (Exception ignored) {
                return null;
            }
            return new AbstractMap.SimpleEntry<>(key, value);
        }

    }
}
