package ir.nimvb.lib.upload.model.s3;

import com.amazonaws.services.s3.model.CompleteMultipartUploadResult;
import ir.nimvb.lib.upload.model.ObjectContext;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@Data
public class S3ObjectContext extends ObjectContext {

    public S3ObjectContext(S3ObjectContext other) {
        this((ObjectContext) other);
        this.uploadId = other.uploadId;
        this.uploadResult = other.uploadResult;
    }

    public S3ObjectContext(ObjectContext other) {
        super(other);
    }

    public S3ObjectContext() {
    }

    public void setUploadResult(CompleteMultipartUploadResult result){
        this.uploadResult = Optional.ofNullable(result);
    }

    private String uploadId;
    private int nextPartId = 0;
    private Optional<CompleteMultipartUploadResult> uploadResult = Optional.empty();
}
