package ir.nimvb.lib.upload.service.protocol.core.patch.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.protocol.common.step.manager.AbstractObjectIdValidationStep;
import ir.nimvb.lib.upload.service.protocol.core.patch.model.RequestContext;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class ObjectIdValidationStep extends AbstractObjectIdValidationStep<RequestContext> {
    public ObjectIdValidationStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(RequestContext.class, request, response, dependencyManager);
    }

    @Override
    protected String getObjectId(StageContext context) {
        RequestContext content = (RequestContext) context.getContent();
        return content.getObjectId();
    }

    @Override
    protected void setObjectContext(StageContext context, Optional<ObjectContext> objectContext) {
        RequestContext content = (RequestContext) context.getContent();
        content.setObjectContext(objectContext);
    }

    @Override
    protected void setStatus(StageContext context, HttpStatus status) {
        RequestContext content = (RequestContext) context.getContent();
        content.setStatus(status);
    }
}
