package ir.nimvb.lib.upload.service.protocol.core.patch.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.protocol.core.patch.model.RequestContext;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StoringRequestContentStep extends AbstractStepManagerWithContentType<RequestContext> {
    public StoringRequestContentStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(RequestContext.class, request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        RequestContext requestContext = content(context);
        ObjectContext objectContext = requestContext.getObjectContext().get();
        long contentLength = requestContext.getContentLength();
        long offset = objectContext.getOffset();
        long length = objectContext.getLength();
        if(offset != length){
            try {
                long writtenBytesCount
                        = dependencyManager.getObjectManager().getWriter(objectContext.getId()).write(request.getInputStream());
                offset = offset + writtenBytesCount;
            } catch (IOException e) {

            }
        }else{
        }
        requestContext.setStatus(HttpStatus.NO_CONTENT);
        response.setHeader(TusHttpHeaders.UPLOAD_OFFSET,String.valueOf(offset));
        chain.next(context);
        return context;
    }
}
