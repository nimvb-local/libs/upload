package ir.nimvb.lib.upload.service.protocol.extension.creation.model;

import ir.nimvb.lib.upload.model.ObjectContext;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
public class CreationExtensionContext {
    @Setter
    HttpStatus status = HttpStatus.BAD_REQUEST;
    @Getter
    ObjectContext context = new ObjectContext();
}
