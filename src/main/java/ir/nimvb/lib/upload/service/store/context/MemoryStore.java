package ir.nimvb.lib.upload.service.store.context;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.nimvb.lib.upload.exception.ObjectContextException;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class MemoryStore implements ObjectContextStore{

    private final TusUploadConfigurationProperties configuration;
    private final ObjectMapper converter;
    private final Map<String,String> store = new HashMap<>();

    @Override
    public ObjectContext save(ObjectContext context) {
        String contextPath = ObjectContextStore.Util.Path.store(this.configuration, context.getId());
        try {
            String json = converter.writeValueAsString(context);
            store.put(contextPath,json);
        }catch (Exception ex){
            throw new ObjectContextException(ex);
        }
        return context;
    }

    @Override
    public Optional<ObjectContext> find(String id) {

        String contextPath = ObjectContextStore.Util.Path.store(this.configuration,id);
        if(!store.containsKey(contextPath)){
            return Optional.empty();
        }
        try {
            String json = store.get(contextPath);
            return Optional.ofNullable(converter.readValue(json, ObjectContext.class));
        }catch (Exception ignored){
            return Optional.empty();
        }
    }

    @Override
    public Set<ObjectContext> findAll() {
        return store.values().stream().map(s -> {
            try {
                return converter.readValue(s,ObjectContext.class);
            } catch (JsonProcessingException ignored) {

            }
            return null;
        }).collect(Collectors.toSet());
    }

    @Override
    public void delete(String id) {
        String contextPath = ObjectContextStore.Util.Path.store(this.configuration,id);
        store.remove(contextPath);
    }
}
