package ir.nimvb.lib.upload.service.store.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.model.s3.S3ObjectContext;
import ir.nimvb.lib.upload.service.store.DataStore;
import ir.nimvb.lib.upload.service.store.result.CreateResult;
import ir.nimvb.lib.upload.service.store.result.WriteResult;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
public class S3DataStore implements DataStore, DataStore.Writer {

    private final AmazonS3 s3Client;
    private final String bucket;
    @Getter
    private int bufferSize = 5 * 1024 * 1024;

    public synchronized void setBufferSize(int size) {
        Assert.isTrue(size >= 1024 * 1024 * 5, "minimum buffer size should be at least 512KB");
        bufferSize = size;
    }

    @Override
    public CreateResult create(ObjectContext context) {
        InitiateMultipartUploadRequest request = new InitiateMultipartUploadRequest(bucket, context.getId()).withObjectMetadata(new ObjectMetadata());
        InitiateMultipartUploadResult result = s3Client.initiateMultipartUpload(request);
        S3ObjectContext objectContext = new S3ObjectContext(context);
        log.info("\r\n\r\n<<<<<< create >>>>>>\r\n");
        log.info("create:"
                + "\r\n\t" + "state => " + "CALLED"
                + "\r\n\t" + "bucket => " + bucket
                + "\r\n\t" + "id => " + objectContext.getId()
                + "\r\n\t" + "upload-id => " + objectContext.getUploadId()
                + "\r\n\t" + "upload-completed => " + objectContext.getUploadResult().isPresent()
        );
        objectContext.setUploadId(result.getUploadId());
        objectContext.setNextPartId(1);
        return new CreateResult(objectContext);
    }

    @Override
    public void delete(ObjectContext context) {
        Assert.notNull(context, "context is null");
        Assert.isInstanceOf(S3ObjectContext.class, context, "s3 object context instance required");
        S3ObjectContext s3ObjectContext = (S3ObjectContext) context;
        log.info("\r\n\r\n<<<<<< delete >>>>>>\r\n");
        log.info("delete:"
                + "\r\n\t" + "state => " + "CALLED"
                + "\r\n\t" + "bucket => " + bucket
                + "\r\n\t" + "id => " + s3ObjectContext.getId()
                + "\r\n\t" + "upload-id => " + s3ObjectContext.getUploadId()
                + "\r\n\t" + "upload-completed => " + s3ObjectContext.getUploadResult().isPresent()
        );
        AbortMultipartUploadRequest request = new AbortMultipartUploadRequest(bucket, context.getId(), ((S3ObjectContext) context).getUploadId());
        s3Client.abortMultipartUpload(request);
    }

    @Override
    public long length(ObjectContext context) {
        Assert.notNull(context, "context is null");
        Assert.isInstanceOf(S3ObjectContext.class, context, "s3 object context instance required");
        S3ObjectContext s3ObjectContext = (S3ObjectContext) context;
        log.info("\r\n\r\n<<<<<< length >>>>>>\r\n");
        log.info("length:"
                + "\r\n\t" + "state => " + "CALLED"
                + "\r\n\t" + "bucket => " + bucket
                + "\r\n\t" + "id => " + s3ObjectContext.getId()
                + "\r\n\t" + "upload-id => " + s3ObjectContext.getUploadId()
                + "\r\n\t" + "upload-completed => " + s3ObjectContext.getUploadResult().isPresent()
        );
        boolean uploadCompleted =  s3ObjectContext.getUploadResult().isPresent();
        // if upload has been already completed, return the object length
        if (uploadCompleted) {
            return context.getLength();
        }
        final Optional<PartListing> result = fetchUploadedParts(bucket, s3ObjectContext.getId(), s3ObjectContext.getUploadId());
        if(result.isPresent()){
            final long offset = offset(result.get().getParts());
            log.info("\r\n\t" +"result => " + offset);
            return offset;
        }
        log.info("\r\n\t" +"result => " + s3ObjectContext.getOffset());
        return s3ObjectContext.getOffset();
    }

    @Override
    public ObjectContext finish(ObjectContext context) {
        Assert.notNull(context, "context is null");
        Assert.isInstanceOf(S3ObjectContext.class, context, "s3 object context instance required");
        S3ObjectContext s3ObjectContext = (S3ObjectContext) context;
        log.info("\r\n\r\n<<<<<< finish >>>>>>\r\n");
        log.info("finish:"
                + "\r\n\t" + "state => " + "CALLED"
                + "\r\n\t" + "bucket => " + bucket
                + "\r\n\t" + "id => " + s3ObjectContext.getId()
                + "\r\n\t" + "upload-id => " + s3ObjectContext.getUploadId()
                + "\r\n\t" + "upload-completed => " + s3ObjectContext.getUploadResult().isPresent()
        );
        final Optional<PartListing> result = fetchUploadedParts(bucket, s3ObjectContext.getId(), s3ObjectContext.getUploadId());
        if(result.isPresent()){
            //PartListing parts = s3Client.listParts(result.get());
            List<PartETag> eTags = result.get().getParts().stream().map(summary -> new PartETag(summary.getPartNumber(), summary.getETag())).collect(Collectors.toList());
            CompleteMultipartUploadRequest completeMultipartUploadRequest = new CompleteMultipartUploadRequest(bucket, s3ObjectContext.getId(), s3ObjectContext.getUploadId(), eTags);
            final CompleteMultipartUploadResult completeMultipartUploadResult = s3Client.completeMultipartUpload(completeMultipartUploadRequest);
            s3ObjectContext.setUploadResult(completeMultipartUploadResult);
        }
//        ListPartsRequest listPartsRequest = new ListPartsRequest(bucket, s3ObjectContext.getId(), s3ObjectContext.getUploadId());

        return s3ObjectContext;
    }

    @Override
    public WriteResult write(ObjectContext context, InputStream stream) {
        Assert.notNull(context, "context is null");
        Assert.isInstanceOf(S3ObjectContext.class, context, "s3 object context instance required");
        S3ObjectContext s3ObjectContext = (S3ObjectContext) context;
        log.info("\r\n\r\n<<<<<< write >>>>>>\r\n");
        log.info("write:"
                + "\r\n\t" + "state => " + "CALLED"
                + "\r\n\t" + "bucket => " + bucket
                + "\r\n\t" + "id => " + s3ObjectContext.getId()
                + "\r\n\t" + "upload-id => " + s3ObjectContext.getUploadId()
                + "\r\n\t" + "upload-completed => " + s3ObjectContext.getUploadResult().isPresent()
                + "\r\n\t" + "offset => " + s3ObjectContext.getOffset()
                + "\r\n\t" + "length => " + s3ObjectContext.getLength()
        );

        final Optional<PartListing> partsResult = fetchUploadedParts(bucket, s3ObjectContext.getId(), s3ObjectContext.getUploadId());
        log.info("\r\n\t" + "parts exists => " + partsResult.isPresent());
        if(partsResult.isPresent()){
            //PartListing parts = partsResult.get();
//            s3ObjectContext.setNextPartId(Math.max(parts.getNextPartNumberMarker(), 0) + 1);
            long offset = s3ObjectContext.getOffset();//offset(parts.getParts());
            long length = s3ObjectContext.getLength();
            long bytesTransferred = 0;
            try (ReadableByteChannel channel = Channels.newChannel(stream);
                 InputStream inputStream = Channels.newInputStream(channel)) {
                while (offset < length) {

                    long currentPartSize = Math.min(bufferSize, length - offset);
                    UploadPartRequest uploadPartRequest =
                            new UploadPartRequest()
                                    .withBucketName(bucket)
                                    .withKey(s3ObjectContext.getId())
                                    .withUploadId(s3ObjectContext.getUploadId())
                                    .withPartNumber(s3ObjectContext.getNextPartId())
                                    .withPartSize(currentPartSize)
                                    .withInputStream(inputStream);
                    uploadPartRequest.getRequestClientOptions().setReadLimit(bufferSize);
                    s3Client.uploadPart(uploadPartRequest);
                    offset += currentPartSize;
                    bytesTransferred += currentPartSize;
                    s3ObjectContext.setNextPartId(s3ObjectContext.getNextPartId() + 1);
                }

            } catch (Exception e) {

                log.error("write:"
                        + "\r\n\t" + "state => " + "EXCEPTION"
                        + "\r\n\t" + "bucket => " + bucket
                        + "\r\n\t" + "id => " + s3ObjectContext.getId()
                        + "\r\n\t" + "upload-id => " + s3ObjectContext.getUploadId()
                        + "\r\n\t" + "offset => " + s3ObjectContext.getUploadId()
                        + "\r\n\t" + "length => " + s3ObjectContext.getUploadId()
                        + "\r\n\t" + "error-message => " + e.getMessage()
                );
                return new WriteResult(s3ObjectContext,0);
                //e.printStackTrace();
            }


            return new WriteResult(s3ObjectContext,bytesTransferred);
        }

        return new WriteResult(s3ObjectContext,0);


    }


    private long offset(List<PartSummary> summaries) {
        log.info("\r\n\r\n<<<<<< offset >>>>>>\r\n");
        AtomicLong counter = new AtomicLong();
        summaries.forEach(summary -> counter.addAndGet(summary.getSize()));
        return counter.get();
    }

    public Optional<PartListing> fetchUploadedParts(String bucket, String id, String uploadId) {
        log.info("\r\n\r\n<<<<<< fetch-summaries >>>>>>\r\n");
        log.info("fetch-summaries:"
                + "\r\n\t" + "state => " + "CALLED"
                + "\r\n\t" + "bucket => " + bucket
                + "\r\n\t" + "id => " + id
                + "\r\n\t" + "upload-id => " + uploadId
        );
        try {
            ListPartsRequest listPartsRequest = new ListPartsRequest(bucket, id, uploadId);
            PartListing parts = s3Client.listParts(listPartsRequest);
            return Optional.ofNullable(parts);

        } catch (Exception ex) {
            log.error("fetch-summaries:"
                    + "\r\n\t" + "state => " + "EXCEPTION"
                    + "\r\n\t" + "bucket => " + bucket
                    + "\r\n\t" + "id => " + id
                    + "\r\n\t" + "upload-id => " + uploadId
                    + "\r\n\t" + "error-message => " + ex.getMessage()
            );
        }
        return Optional.empty();
    }
}
