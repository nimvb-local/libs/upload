package ir.nimvb.lib.upload.service.store.request;

import ir.nimvb.lib.upload.model.ObjectContext;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

/**
 * Immutable base class for all requests send to the object storage or context manager
 */
@RequiredArgsConstructor
public class Request {
    private final ObjectContext context;
    public Optional<ObjectContext> getContext() {
        try {
            return Optional.ofNullable(this.context.clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
