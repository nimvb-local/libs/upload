package ir.nimvb.lib.upload.service.store.context;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.nimvb.lib.upload.exception.ObjectContextException;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
import lombok.RequiredArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@RequiredArgsConstructor
public class FileStore implements ObjectContextStore {

    private final TusUploadConfigurationProperties configuration;
    private final ObjectMapper converter;


    @Override
    public ObjectContext save(ObjectContext context) {
        String contextPath = ObjectContextStore.Util.Path.store(this.configuration, context.getId());
        File contextFile = new File(contextPath);
        try {
            converter.writeValue(contextFile, context);
        } catch (Exception ex) {
            throw new ObjectContextException(ex);
        }
        return context;
    }

    @Override
    public Optional<ObjectContext> find(String id) {
        String contextPath = ObjectContextStore.Util.Path.store(this.configuration, id);
        File contextFile = new File(contextPath);
        if (!contextFile.exists()) {
            return Optional.empty();
        }
        try {
            return Optional.ofNullable(converter.readValue(contextFile, ObjectContext.class));
        } catch (Exception ignored) {
            return Optional.empty();
        }
    }

    @Override
    public Set<ObjectContext> findAll() {
        File baseDir = new File(configuration.getFolder());
        if (baseDir.isDirectory()) {
            return (
                    Arrays
                            .stream(Objects
                                    .requireNonNull(baseDir
                                            .listFiles((dir, name) -> {
                                                if (name.endsWith(".info")) {
                                                    try {
                                                        File candidate = new File((dir.getCanonicalPath() + File.separator + name));
                                                        if (!candidate.isDirectory()) {
                                                            return true;
                                                        }
                                                    } catch (IOException ignored) {

                                                    }
                                                }
                                                return false;
                                            })))
                            .map(file -> {
                                try {
                                    return (converter.readValue(file, ObjectContext.class));
                                } catch (IOException ignored) {

                                }
                                return null;
                            })
                            .filter(Objects::nonNull)
                            .collect(Collectors.toSet()));
        }
        return Collections.emptySet();
    }

    @Override
    public void delete(String id) {
        String contextPath = ObjectContextStore.Util.Path.store(this.configuration, id);
        File contextFile = new File(contextPath);
        contextFile.delete();
    }
}
