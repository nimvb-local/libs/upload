package ir.nimvb.lib.upload.service.protocol.core.patch.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.common.util.Request;
import ir.nimvb.lib.upload.service.protocol.core.patch.model.RequestContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class ContentLengthValidationStep extends AbstractStepManagerWithContentType<RequestContext> {
    public ContentLengthValidationStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(RequestContext.class, request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        RequestContext requestContext = content(context);
        Optional<String> contentLengthValue = Request.of(request).headers().value(HttpHeaders.CONTENT_LENGTH);
        ObjectContext objectContext = requestContext.getObjectContext().get();
        if(contentLengthValue.isEmpty()){
            requestContext.setContentLength(objectContext.getLength() - objectContext.getOffset());
        }else{
            try {
                Long contentLength = Long.valueOf(contentLengthValue.get());
                long offset = requestContext.getObjectContext().get().getOffset();
                long length = requestContext.getObjectContext().get().getLength();
                if((offset + contentLength) > length){
                    throw new RuntimeException();
                }
                requestContext.setContentLength(contentLength);
            } catch (Exception e) {
                requestContext.setStatus(HttpStatus.CONFLICT);
                return context;
            }
        }
        chain.next(context);
        return context;
    }
}
