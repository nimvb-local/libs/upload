package ir.nimvb.lib.upload.service.manager;

import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.service.store.DataStore;
import ir.nimvb.lib.upload.service.store.context.ObjectContextStore;
import ir.nimvb.lib.upload.service.store.result.WriteResult;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

@RequiredArgsConstructor
public class ObjectManager {
    @NonNull
    private final DataStore dataStore;
    @NonNull
    private final DataStore.Writer writer;
    @NonNull
    private final DataStore.Reader reader;
    @NonNull
    private final ObjectContextStore contextStore;


    public void create(ObjectContext context) {
        ObjectContext result = dataStore.create(context).getContext();
        contextStore.save(result);
    }

    public Optional<ObjectContext> find(String id) {
        Optional<ObjectContext> context = contextStore.find(id);
        if(context.isPresent()) {
            long length = dataStore.length(context.get());
            if (length == -1) {
                context = Optional.empty();
            }
            context.ifPresent(objectContext -> objectContext.setOffset(length));
        }
        return context;
    }

    public void delete(String id){
        Optional<ObjectContext> context = contextStore.find(id);
        if(context.isPresent()){
            contextStore.delete(id);
            dataStore.delete(context.get());
        }
    }

    public Writer getWriter(String id) {
        Optional<ObjectContext> objectContext = find(id);
        if (objectContext.isPresent()) {
            ObjectContext context = objectContext.get();
            return new Writer() {
                @Override
                public long write(InputStream stream) {
                    final WriteResult writeResult = ObjectManager.this.writer.write(context, stream);
                    ObjectManager.this.contextStore.save(writeResult.getContext());
                    final long writtenBytes = (long) writeResult.getCount();
                    if(context.getOffset() + writtenBytes >= context.getLength()){
                        final ObjectContext finish = ObjectManager.this.dataStore.finish(context);
                        ObjectManager.this.contextStore.save(finish);
                    }
                    return writtenBytes;
                }
            };
        }
        return new Writer() {
            @Override
            public long write(InputStream stream) {
                return -1;
            }
        };
    }

    public Reader getReader(String id){
        Optional<ObjectContext> objectContext = find(id);
        if(objectContext.isPresent()){
            ObjectContext context = objectContext.get();
            return new Reader() {
                @Override
                public InputStream stream() throws IOException {
                    return ObjectManager.this.reader.getInputStream(id);
                }
            };
        }


        return new Reader() {
            @Override
            public InputStream stream() {
                return InputStream.nullInputStream();
            }
        };
    }

    public interface Writer {

        public long write(InputStream stream);

    }

    public interface Reader {
        InputStream stream() throws IOException;
    }


}
