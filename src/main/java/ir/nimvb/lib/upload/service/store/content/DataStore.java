//package ir.nimvb.lib.upload.service.store.content;
//
//import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.File;
//import java.io.InputStream;
//import java.nio.ByteBuffer;
//
//public interface DataStore {
//
//    void create(String id);
//
//    void delete(String id);
//
//    interface Writer {
//        long write(String id, InputStream stream, long offset, long length);
//    }
//
//    interface Reader {
//        byte[] read(String id, long offset, long length);
//    }
//
//    public interface Util {
//        public interface Path {
//            public static String store(TusUploadConfigurationProperties properties, String id){
//                return properties.getFolder() + File.separator + id + ".bin";
//            }
//        }
//    }
//}
