//package ir.nimvb.lib.upload.service.protocol.extension.creation;
//
//import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
//import ir.nimvb.lib.upload.model.Metadata;
//import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
//import ir.nimvb.lib.upload.service.manager.ObjectManager;
//import ir.nimvb.lib.upload.service.protocol.Http;
//import lombok.RequiredArgsConstructor;
//import org.springframework.http.HttpStatus;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@RequiredArgsConstructor
//public class HandleUploadMetadata implements Handler{
//    private final HttpServletRequest request;
//    private final HttpServletResponse response;
//    private final TusUploadConfigurationProperties properties;
//    private ObjectManager manager;
//    @Override
//    public HttpStatus handle() {
//        if(Http.Util.Request.isHeaderExists(request, TusHttpHeaders.UPLOAD_METADATA)){
//            String encodedMetadata = request.getHeader(TusHttpHeaders.UPLOAD_METADATA);
//            Metadata metadata = Metadata.Decoder.decode(encodedMetadata);
//            manager.find();
//
//        }
//        return HttpStatus.CREATED;
//    }
//}
