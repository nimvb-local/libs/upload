//package ir.nimvb.lib.upload.service.store.content;
//
//import ir.nimvb.lib.upload.exception.DataStoreAlreadyExistsException;
//import ir.nimvb.lib.upload.exception.DataStoreInvalidStoreException;
//import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
//import lombok.RequiredArgsConstructor;
//
//import java.io.*;
//import java.nio.channels.Channels;
//import java.nio.channels.FileChannel;
//import java.nio.channels.ReadableByteChannel;
//import java.nio.channels.WritableByteChannel;
//
//@RequiredArgsConstructor
//public class FileDataStore implements DataStore, DataStore.Writer ,DataStore.Reader{
//
//    private final TusUploadConfigurationProperties properties;
//
//
//    @Override
//    public void create(String id) {
//        File dest = new File(DataStore.Util.Path.store(properties, id));
//        try {
//            boolean created = dest.createNewFile();
//            if (!created) {
//                throw new DataStoreAlreadyExistsException();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @Override
//    public void delete(String id) {
//        File dest = new File(DataStore.Util.Path.store(properties, id));
//        if (!dest.exists() || dest.isDirectory()) {
//            return;
//        }
//        dest.delete();
//    }
//
//    @Override
//    public long write(String id, InputStream stream, long offset, long length) {
//        File dest = new File(DataStore.Util.Path.store(properties, id));
//        if (!dest.exists() || !dest.isFile() || !dest.canWrite()) {
//            throw new DataStoreInvalidStoreException();
//        }
//        try (RandomAccessFile fd = new RandomAccessFile(dest, "rw");
//             FileChannel channel = fd.getChannel();
//             ReadableByteChannel input = Channels.newChannel(stream);
//        ) {
//            return channel.transferFrom(input, offset, length);
//        } catch (FileNotFoundException e) {
//            throw new DataStoreInvalidStoreException();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        return 0;
//    }
//
//    @Override
//    public byte[] read(String id, long offset, long length) {
//        File source = new File(DataStore.Util.Path.store(properties, id));
//        if (!source.exists() || !source.isFile() || !source.canRead()) {
//            throw new DataStoreInvalidStoreException();
//        }
//        try (RandomAccessFile fd = new RandomAccessFile(source, "r");
//             ByteArrayOutputStream sink = new ByteArrayOutputStream();
//             WritableByteChannel dest = Channels.newChannel(sink);
//             FileChannel channel = fd.getChannel();
//        ) {
//            channel.transferTo(offset,length,dest);
//            return sink.toByteArray();
//        } catch (FileNotFoundException e) {
//            throw new DataStoreInvalidStoreException();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//        }
//
//        return new byte[0];
//    }
//}
