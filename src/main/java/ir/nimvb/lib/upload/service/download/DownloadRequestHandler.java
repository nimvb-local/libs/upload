package ir.nimvb.lib.upload.service.download;

import com.nimvb.lib.pattern.behavioral.chain.ChainExecutionReport;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.request.AbstractRequestHandler;
import ir.nimvb.lib.upload.service.download.model.DownloadRequestContext;
import ir.nimvb.lib.upload.service.download.step.builder.DownloadRequestStepBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Map;

public class DownloadRequestHandler extends AbstractRequestHandler {

    private HttpStatus status = HttpStatus.NOT_FOUND;


    public DownloadRequestHandler(DependencyManager dependencyManager) {
        super(dependencyManager);
    }

    @Override
    protected HttpStatus status() {
        return status;
    }

    @Override
    protected void process(HttpServletRequest request, HttpServletResponse response) {
        StageContext context =  new StageContext();
        context.setContent(new DownloadRequestContext(getId(request)));
        ChainExecutionReport report = DownloadRequestStepBuilder.INSTANCE.build(request, response, dependencyManager).execute(context);
        status = ((DownloadRequestContext) report.content()).getStatus();
        response.setStatus(status.value());
    }
    public String getId(HttpServletRequest request) {
        return ((Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE)).get("id");
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.GET;
    }

    @Override
    public String path() {
        return "/{id}";
    }

    @Override
    public String[] pathVariables() {
        return Collections.singletonList("id").toArray(new String[0]);
    }
}
