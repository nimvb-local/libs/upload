package ir.nimvb.lib.upload.service.protocol.extension.termination.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.protocol.common.step.manager.AbstractObjectIdValidationStep;
import ir.nimvb.lib.upload.service.protocol.extension.termination.model.TerminationExtensionContext;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class ObjectIdValidationStep extends AbstractObjectIdValidationStep<TerminationExtensionContext> {
    public ObjectIdValidationStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(TerminationExtensionContext.class, request, response, dependencyManager);
    }

    @Override
    protected String getObjectId(StageContext context) {
        TerminationExtensionContext content = content(context);
        return content.getObjectId();
    }

    @Override
    protected void setObjectContext(StageContext context, Optional<ObjectContext> objectContext) {
        TerminationExtensionContext content = content(context);
        content.setObjectContext(objectContext);
    }

    @Override
    protected void setStatus(StageContext context, HttpStatus status) {
        TerminationExtensionContext content = content(context);
        content.setStatus(status);
    }
}
