package ir.nimvb.lib.upload.service.store;

import ir.nimvb.lib.upload.exception.DataStoreAlreadyExistsException;
import ir.nimvb.lib.upload.exception.DataStoreInvalidStoreException;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
import ir.nimvb.lib.upload.service.store.result.CreateResult;
import ir.nimvb.lib.upload.service.store.result.WriteResult;
import lombok.RequiredArgsConstructor;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

@RequiredArgsConstructor
public class FileDataStore implements DataStore, DataStore.Writer, DataStore.Reader {

    private final TusUploadConfigurationProperties properties;


    @Override
    public CreateResult create(ObjectContext context) {
        File dest = new File(DataStore.Util.Path.store(properties, context.getId()));
        try {
            boolean created = dest.createNewFile();
            if (!created) {
                throw new DataStoreAlreadyExistsException();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new CreateResult(context);
    }

    @Override
    public void delete(ObjectContext context) {
        File dest = new File(DataStore.Util.Path.store(properties, context.getId()));
        if (!dest.exists() || dest.isDirectory()) {
            return;
        }
        File deleted = new File(DataStore.Util.Path.deleted(properties, context.getId()));
        try {
            Files.copy(dest.toPath(), deleted.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception ex) {

        } finally {
            dest.delete();
        }

    }

    @Override
    public long length(ObjectContext context) {
        File dest = new File(DataStore.Util.Path.store(properties, context.getId()));
        if (!dest.exists() || dest.isDirectory()) {
            return -1;
        }
        return dest.length();
    }

    @Override
    public ObjectContext finish(ObjectContext context) {
        return context;
    }

    @Override
    public WriteResult write(ObjectContext context, InputStream stream) {
        File dest = new File(DataStore.Util.Path.store(properties, context.getId()));
        if (!dest.exists() || !dest.isFile() || !dest.canWrite()) {
            throw new DataStoreInvalidStoreException();
        }
        try (RandomAccessFile fd = new RandomAccessFile(dest, "rw");
             FileChannel channel = fd.getChannel();
             ReadableByteChannel input = Channels.newChannel(stream);
        ) {
            final long count = channel.transferFrom(input, context.getOffset(), context.getLength());
            return new WriteResult(context, count);
        } catch (FileNotFoundException e) {
            throw new DataStoreInvalidStoreException();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
        return new WriteResult(context, 0);
    }

    @Override
    public byte[] read(String id, long offset, long length) {
        File source = new File(DataStore.Util.Path.store(properties, id));
        if (!source.exists() || !source.isFile() || !source.canRead()) {
            throw new DataStoreInvalidStoreException();
        }
        try (RandomAccessFile fd = new RandomAccessFile(source, "r");
             ByteArrayOutputStream sink = new ByteArrayOutputStream();
             WritableByteChannel dest = Channels.newChannel(sink);
             FileChannel channel = fd.getChannel();
        ) {
            channel.transferTo(offset, length, dest);
            return sink.toByteArray();
        } catch (FileNotFoundException e) {
            throw new DataStoreInvalidStoreException();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }

        return new byte[0];
    }

    @Override
    public InputStream getInputStream(String id) throws IOException {
        File source = new File(DataStore.Util.Path.store(properties, id));
        if (!source.exists() || !source.isFile() || !source.canRead()) {
            throw new DataStoreInvalidStoreException();
        }
        return new FileInputStream(source);
    }
}
