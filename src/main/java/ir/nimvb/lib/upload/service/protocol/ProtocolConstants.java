package ir.nimvb.lib.upload.service.protocol;

import java.util.stream.Stream;

public class ProtocolConstants {
    public static final String VERSION = "1.0.0";
    public static final String EXTENSIONS = Stream.of("creation","termination").reduce((s, s2) -> String.format("%s,%s",s,s2)).get();
}
