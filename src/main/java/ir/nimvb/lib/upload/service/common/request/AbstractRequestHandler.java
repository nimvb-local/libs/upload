package ir.nimvb.lib.upload.service.common.request;

import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
public abstract class AbstractRequestHandler implements RequestHandler {
    protected final DependencyManager dependencyManager;

    public void execute(HttpServletRequest request,HttpServletResponse response){
        process(request,response);
    }

    protected abstract HttpStatus status();
    protected abstract void process(HttpServletRequest request,HttpServletResponse response);

    @Override
    public AbstractRequestHandler handler() {
        return this;
    }
}
