package ir.nimvb.lib.upload.service.protocol.core.patch.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.common.util.Request;
import ir.nimvb.lib.upload.service.protocol.core.patch.model.RequestContext;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class UploadOffsetScanningStep extends AbstractStepManagerWithContentType<RequestContext> {
    public UploadOffsetScanningStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(RequestContext.class, request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        RequestContext content = content(context);
        boolean uploadOffsetExists = Request.of(request).headers().exists(TusHttpHeaders.UPLOAD_OFFSET);
        if(!uploadOffsetExists){
            content.setStatus(HttpStatus.NOT_FOUND);
            return context;
        }
        Optional<String> value = Request.of(request).headers().value(TusHttpHeaders.UPLOAD_OFFSET);
        if(value.isEmpty()){
            content.setStatus(HttpStatus.CONFLICT);
        }
        long offset = content.getObjectContext().get().getOffset();
        try {
            long clientUploadOffset = Long.parseLong(value.get());
            if(clientUploadOffset != offset){
                throw new RuntimeException();
            }
        } catch (Exception e) {
            content.setStatus(HttpStatus.CONFLICT);
            return context;
        }
        chain.next(context);
        return context;
    }
}
