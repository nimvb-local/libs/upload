package ir.nimvb.lib.upload.service.common.request;

import org.springframework.http.HttpMethod;

public interface RequestHandler {

    HttpMethod method();
    String path();
    String[] pathVariables();
    AbstractRequestHandler handler();
}
