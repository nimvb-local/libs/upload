package ir.nimvb.lib.upload.service.protocol.core.patch.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.protocol.tus.http.TusMediaType;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManager;
import ir.nimvb.lib.upload.service.protocol.core.patch.model.RequestContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContentTypeValidationStep extends AbstractStepManager {
    public ContentTypeValidationStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(request, response, dependencyManager);
    }

    @Override
    protected <T> Class<T> contentType() {
        return (Class<T>) RequestContext.class;
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        RequestContext requestContext = (RequestContext) context.getContent();
        String contentType = request.getHeader(HttpHeaders.CONTENT_TYPE);
        if(contentType == null){
            requestContext.setStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
            return context;
        }
        if(!contentType.equals(TusMediaType.APPLICATION_OFFSET_OCTET_STREAM_VALUE)){
            requestContext.setStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
            return context;
        }
        chain.next(context);
        return context;
    }
}
