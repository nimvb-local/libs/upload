package ir.nimvb.lib.upload.service.download.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.download.model.DownloadRequestContext;
import ir.nimvb.lib.upload.service.manager.ObjectManager;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.async.AsyncWebRequest;
import org.springframework.web.context.request.async.WebAsyncManager;
import org.springframework.web.context.request.async.WebAsyncUtils;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;

public class ResponseBodyBuildingStep extends AbstractStepManagerWithContentType<DownloadRequestContext> {
    public ResponseBodyBuildingStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(DownloadRequestContext.class, request, response, dependencyManager);
    }

    @SneakyThrows
    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        DownloadRequestContext content = content(context);
        ObjectManager.Reader reader = dependencyManager.getObjectManager().getReader(content.getObjectId());

        AsyncWebRequest asyncWebRequest = WebAsyncUtils.createAsyncWebRequest(request, response);
        asyncWebRequest.setTimeout(null);
        WebAsyncManager asyncManager = WebAsyncUtils
                .getAsyncManager(new ServletWebRequest(request, response));
        asyncManager.setAsyncWebRequest(asyncWebRequest);
        asyncManager.setTaskExecutor(dependencyManager.concurrency().taskExecutor());
        asyncManager
                .startCallableProcessing
                        (
                                new StreamingResponseBodyTask
                                        (
                                                response.getOutputStream(),
                                                new StreamingResponseBodyHandler(reader.stream())
                                        )
                        );
        chain.next(context);
        return context;
    }


    @RequiredArgsConstructor
    private static final class StreamingResponseBodyTask implements Callable<Void> {

        @NonNull
        private final OutputStream outputStream;
        @NonNull
        private final StreamingResponseBody streamingResponseBody;

        @Override
        public Void call() throws Exception {
            streamingResponseBody.writeTo(outputStream);
            outputStream.flush();
            return null;
        }
    }

    @RequiredArgsConstructor
    private static final class StreamingResponseBodyHandler implements StreamingResponseBody {
        @NonNull
        private final InputStream inputStream;

        @Override
        public void writeTo(OutputStream outputStream) throws IOException {
            byte[] buffer = new byte[1024 * 1024 * 2];
            while (true) {
                int count = inputStream.read(buffer, 0, buffer.length);
                if (count <= -1) {
                    break;
                }
                outputStream.write(buffer, 0, count);
            }
        }
    }
}
