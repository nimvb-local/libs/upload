package ir.nimvb.lib.upload.service.protocol.core.patch.step.builder;

import com.nimvb.lib.pattern.behavioral.chain.Chain;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.builder.ExecutorBuilder;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.HttpRequestManagementStepsBuilder;
import ir.nimvb.lib.upload.service.protocol.core.patch.step.manager.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PatchStepBuilder implements HttpRequestManagementStepsBuilder {
    public final static PatchStepBuilder INSTANCE = new PatchStepBuilder();

    private PatchStepBuilder(){}
    @Override
    public ChainExecutor build(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        ExecutorBuilder builder = Chain.INSTANCE.builder();
        builder.add(new ContentTypeValidationStep(request,response, dependencyManager));
        builder.add(new ObjectIdValidationStep(request,response, dependencyManager));
        builder.add(new UploadOffsetScanningStep(request,response, dependencyManager));
        builder.add(new ContentLengthValidationStep(request,response, dependencyManager));
        builder.add(new StoringRequestContentStep(request,response, dependencyManager));
        return builder.build();
    }
}
