package ir.nimvb.lib.upload.service.protocol.core.patch.model;

import ir.nimvb.lib.upload.model.ObjectContext;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Optional;

@RequiredArgsConstructor
@Getter
public class RequestContext {
    @Getter
    private final String objectId;
    @Setter
    private HttpStatus status = HttpStatus.NOT_FOUND;
    @Setter
    private Optional<ObjectContext> objectContext;
    @Setter
    private long contentLength;
}
