package ir.nimvb.lib.upload.service.protocol.extension.creation.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.Stage;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.protocol.extension.creation.model.CreationExtensionContext;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class UploadLengthManagementStep extends AbstractStepManagerWithContentType<CreationExtensionContext> implements Stage {
    public UploadLengthManagementStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(CreationExtensionContext.class,request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        CreationExtensionContext content  = context.getContent();
        content.getContext().setUploadLength(request.getHeader(TusHttpHeaders.UPLOAD_LENGTH));
        if(content.getContext().getUploadLength().isEmpty()){
            content.setStatus(HttpStatus.BAD_REQUEST);
            return context;
        }
        long maxUploadSize = dependencyManager.getProperties().getRequest().getSize().getMaximum();
        long uploadLength = Long.parseLong(content.getContext().getUploadLength().get());
        if (maxUploadSize > 0) {
            if (uploadLength > maxUploadSize) {
                content.setStatus(HttpStatus.PAYLOAD_TOO_LARGE);
                return context;
            }
        }
        content.setStatus(HttpStatus.CREATED);
        chain.next(context);
        return context;
    }
}
