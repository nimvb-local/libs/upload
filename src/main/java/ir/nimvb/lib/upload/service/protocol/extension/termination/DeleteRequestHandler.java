package ir.nimvb.lib.upload.service.protocol.extension.termination;

import com.nimvb.lib.pattern.behavioral.chain.ChainExecutionReport;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.protocol.AbstractTusProtocolRequestHandler;
import ir.nimvb.lib.upload.service.protocol.extension.Extension;
import ir.nimvb.lib.upload.service.protocol.extension.termination.model.TerminationExtensionContext;
import ir.nimvb.lib.upload.service.protocol.extension.termination.step.builder.TerminationStepBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerMapping;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

public class DeleteRequestHandler extends AbstractTusProtocolRequestHandler implements Extension {

    private HttpStatus status = HttpStatus.NOT_FOUND;

    public DeleteRequestHandler(DependencyManager dependencyManager) {
        super(dependencyManager);
    }

    @Override
    protected HttpStatus status() {
        return status;
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.DELETE;
    }

    @Override
    public String path() {
        return "/{id}";
    }

    @Override
    public String[] pathVariables() {
        return Collections.singletonList("id").toArray(new String[0]);
    }

    @Override
    protected void process() {
        StageContext context = new StageContext();
        TerminationExtensionContext content = new TerminationExtensionContext(getId());
        context.setContent(content);
        ChainExecutionReport report = TerminationStepBuilder.INSTANCE.build(request, response, dependencyManager).execute(context);
        status = ((TerminationExtensionContext) report.content()).getStatus();

    }

    public String getId() {
        return ((Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE)).get("id");
    }

    @Override
    public String name() {
        return "termination";
    }
}
