package ir.nimvb.lib.upload.service.protocol.extension.creation.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.Stage;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import ir.nimvb.lib.upload.model.Metadata;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.common.util.Request;
import ir.nimvb.lib.upload.service.protocol.extension.creation.model.CreationExtensionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;


public class UploadMetadataManagementStep extends AbstractStepManagerWithContentType<CreationExtensionContext> implements Stage {


    public UploadMetadataManagementStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(CreationExtensionContext.class, request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        CreationExtensionContext content = content(context);
        Optional<String> value = Request.of(request).headers().value(TusHttpHeaders.UPLOAD_METADATA);
        if (value.isPresent()) {
            Metadata metadata = Metadata.Decoder.decode(value.get());
            content.getContext().setMetadata(metadata);
        }
        chain.next(context);
        return context;
    }
}
