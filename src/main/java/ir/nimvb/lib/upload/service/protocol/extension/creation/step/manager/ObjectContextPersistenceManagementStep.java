package ir.nimvb.lib.upload.service.protocol.extension.creation.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.Stage;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.protocol.extension.creation.model.CreationExtensionContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ObjectContextPersistenceManagementStep extends AbstractStepManagerWithContentType<CreationExtensionContext> implements Stage {


    public ObjectContextPersistenceManagementStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(CreationExtensionContext.class, request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        try {
            dependencyManager.getObjectManager().create(((CreationExtensionContext) context.getContent()).getContext());
            CreationExtensionContext content = context.getContent();
            StringBuffer url = request.getRequestURL();
            String uri = url + "/" + ((CreationExtensionContext) context.getContent()).getContext().getId();
            response.setHeader(HttpHeaders.LOCATION,uri);

        } catch (Exception e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
        }
        chain.next(context);
        return context;
    }
}
