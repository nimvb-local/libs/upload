package ir.nimvb.lib.upload.service.common.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractStepManagerWithContentType<T> extends AbstractStepManager {
    private final Class<T> contentType;
    public AbstractStepManagerWithContentType(Class<T> contentType,HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(request, response, dependencyManager);
        this.contentType = contentType;
    }

    @Override
    protected <T> Class<T> contentType() {
        return (Class<T>) contentType;
    }

    protected T content(StageContext context) {
        T content = (T) context.getContent();
        return content;
    }
}
