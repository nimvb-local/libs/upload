package ir.nimvb.lib.upload.service.protocol.core.options.step.builder;

import com.nimvb.lib.pattern.behavioral.chain.Chain;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.builder.ExecutorBuilder;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.HttpRequestManagementStepsBuilder;
import ir.nimvb.lib.upload.service.protocol.core.options.step.manager.TusExtensionsScanningStep;
import ir.nimvb.lib.upload.service.protocol.core.options.step.manager.TusVersionScanningStep;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OptionsStepBuilder implements HttpRequestManagementStepsBuilder {
    public static final OptionsStepBuilder INSTANCE = new OptionsStepBuilder();

    private OptionsStepBuilder(){}

    @Override
    public ChainExecutor build(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        ExecutorBuilder builder = Chain.INSTANCE.builder();
        builder.add(new TusVersionScanningStep(request,response,dependencyManager));
        builder.add(new TusVersionScanningStep(request,response,dependencyManager));
        builder.add(new TusExtensionsScanningStep(request,response,dependencyManager));
        return builder.build();
    }
}
