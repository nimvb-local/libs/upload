package ir.nimvb.lib.upload.service.protocol.extension;

public interface Extension {
    String name();
}
