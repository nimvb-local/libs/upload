package ir.nimvb.lib.upload.service.protocol.core.options.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@RequiredArgsConstructor
@Getter
public class OptionsRequestContext {
    @Setter
    private HttpStatus status = HttpStatus.NO_CONTENT;
    @Setter
    private String extensions = "";

}
