package ir.nimvb.lib.upload.service.protocol.core.patch;

import com.nimvb.lib.pattern.behavioral.chain.ChainExecutionReport;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.protocol.AbstractTusProtocolRequestHandler;
import ir.nimvb.lib.upload.service.protocol.core.patch.model.RequestContext;
import ir.nimvb.lib.upload.service.protocol.core.patch.step.builder.PatchStepBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerMapping;

import java.util.Collections;
import java.util.Map;

public class PatchRequestHandler extends AbstractTusProtocolRequestHandler {

    private HttpStatus status = HttpStatus.NOT_FOUND;

    public PatchRequestHandler(DependencyManager dependencyManager) {
        super(dependencyManager);
    }

    @Override
    protected HttpStatus status() {
        return status;
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.PATCH;
    }

    @Override
    public String path() {
        return "/{id}";
    }

    @Override
    public String[] pathVariables() {
        return Collections.singletonList("id").toArray(new String[0]);
    }

    @Override
    protected void process() {
        StageContext context = new StageContext();
        RequestContext content = new RequestContext(getId());
        context.setContent(content);
        ChainExecutionReport report = PatchStepBuilder.INSTANCE.build(request, response, dependencyManager).execute(context);
        status = ((RequestContext) report.content()).getStatus();

    }

    public String getId() {
        return ((Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE)).get("id");
    }
}
