package ir.nimvb.lib.upload.service.protocol.common.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public abstract class AbstractObjectIdValidationStep<T> extends AbstractStepManagerWithContentType<T> {


    public AbstractObjectIdValidationStep(Class<T> contentType, HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(contentType, request, response, dependencyManager);
    }

    protected abstract String getObjectId(StageContext context);

    protected abstract void setObjectContext(StageContext context, Optional<ObjectContext> objectContext);

    protected abstract void setStatus(StageContext context, HttpStatus status);

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        String objectId = getObjectId(context);
        Optional<ObjectContext> objectContext = dependencyManager.getObjectManager().find(objectId);
        setObjectContext(context, objectContext);
        if (objectContext.isPresent()) {
            setStatus(context,HttpStatus.OK);
            chain.next(context);
        } else {
            setStatus(context,HttpStatus.NOT_FOUND);
        }
        return context;
    }
}
