package ir.nimvb.lib.upload.service.common.util;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public interface Request {


    static Operations of(HttpServletRequest request) {
        return () -> new Headers() {
            @Override
            public boolean exists(String header) {
                return request.getHeader(header) != null;
            }

            @Override
            public Optional<String> value(String header) {
                return Optional.ofNullable(request.getHeader(header));
            }
        };
    }

    interface Operations {
        Headers headers();
    }

    interface Headers {
        boolean exists(String header);

        Optional<String> value(String header);
    }


}
