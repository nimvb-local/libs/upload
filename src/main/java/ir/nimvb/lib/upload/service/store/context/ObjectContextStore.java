package ir.nimvb.lib.upload.service.store.context;

import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;

import java.io.File;
import java.util.Optional;
import java.util.Set;

public interface ObjectContextStore {

    ObjectContext save(ObjectContext context);
    Optional<ObjectContext> find(String id);
    Set<ObjectContext> findAll();
    void delete(String id);

    public interface Util {
        public interface Path {
            public static String store(TusUploadConfigurationProperties properties, String id){
                return properties.getFolder() + File.separator + id + ".info";
            }
        }
    }

}
