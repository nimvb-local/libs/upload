package ir.nimvb.lib.upload.service.converter.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import ir.nimvb.lib.upload.model.Metadata;

import java.io.IOException;

public class MetadataDeserializer extends StdDeserializer<Metadata> {

    public MetadataDeserializer() {
        this(Metadata.class);
    }
    protected MetadataDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Metadata deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        String value = null;
        if(treeNode != null){
            if(treeNode instanceof ObjectNode) {
                value = ((ObjectNode) treeNode).get("value").asText();
            }
        }
        Metadata metadata = Metadata.Decoder.decode(value);
        if(metadata == null){
            metadata = new Metadata();
        }
        return metadata;
    }
}
