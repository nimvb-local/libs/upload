package ir.nimvb.lib.upload.service.protocol.core.options.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.controller.model.DispatcherContext;
import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.request.RequestHandler;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.protocol.core.options.model.OptionsRequestContext;
import ir.nimvb.lib.upload.service.protocol.extension.Extension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;

public class TusExtensionsScanningStep extends AbstractStepManagerWithContentType<OptionsRequestContext> {
    public TusExtensionsScanningStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(OptionsRequestContext.class, request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        OptionsRequestContext content = content(context);
        DispatcherContext dispatcherContext = dependencyManager.getDispatcherContext();
        Collection<RequestHandler> handlers = dispatcherContext.getHandlers();
        Collection<String> extensions = new ArrayList<>();
        for (RequestHandler handler : handlers) {
            if (handler instanceof Extension) {
                Extension extension = (Extension) handler;
                extensions.add(extension.name());
            }
        }
        String availableExtensions = extensions.stream().reduce((s, s2) -> String.format("%s, %s", s, s2)).orElse("");
        content.setExtensions(availableExtensions);
        response.setHeader(TusHttpHeaders.TUS_EXTENSION, availableExtensions);
        chain.next(context);
        return context;
    }
}
