package ir.nimvb.lib.upload.service.protocol.core.head.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.protocol.common.step.manager.AbstractObjectIdValidationStep;
import ir.nimvb.lib.upload.service.protocol.core.head.model.RequestContext;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class ObjectIdValidationStep extends AbstractObjectIdValidationStep<RequestContext> {
    public ObjectIdValidationStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(RequestContext.class, request, response, dependencyManager);
    }

    @Override
    protected String getObjectId(StageContext context) {
        RequestContext requestContext = (RequestContext) context.getContent();
        return requestContext.getObjectId();
    }

    @Override
    protected void setObjectContext(StageContext context, Optional<ObjectContext> objectContext) {
        RequestContext requestContext = (RequestContext) context.getContent();
        requestContext.setObjectContext(objectContext);
    }

    @Override
    protected void setStatus(StageContext context, HttpStatus status) {
        RequestContext requestContext = (RequestContext) context.getContent();
        requestContext.setStatus(status);
    }


//    public ObjectIdValidationStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
//        super(request, response, dependencyManager);
//    }
//
//    @Override
//    protected <T> Class<T> contentType() {
//        return (Class<T>) RequestContext.class;
//    }
//
//    @Override
//    public StageContext run(StageContext context, LayerExecutor chain) {
//        RequestContext requestContext = (RequestContext) context.getContent();
//        String objectId = requestContext.getObjectId();
//        requestContext.setObjectContext(dependencyManager.getObjectManager().find(objectId));
//        if (requestContext.getObjectContext().isPresent()) {
//            requestContext.setStatus(HttpStatus.OK);
//            chain.next(context);
//        } else {
//            requestContext.setStatus(HttpStatus.NOT_FOUND);
//        }
//
//        return context;
//    }
}
