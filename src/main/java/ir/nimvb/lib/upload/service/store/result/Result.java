package ir.nimvb.lib.upload.service.store.result;

import ir.nimvb.lib.upload.model.ObjectContext;
import lombok.Getter;
import lombok.RequiredArgsConstructor;



@RequiredArgsConstructor
@Getter
public class Result {
    private final ObjectContext context;
}
