package ir.nimvb.lib.upload.service.protocol.factory;

import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.manager.ObjectManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface RequestHandlerConfigurer {
    public static final RequestHandlerConfigurer INSTANCE = new RequestHandlerConfigurerImpl();

    RequestConfigurer configure();

    public interface RequestConfigurer {
        ResponseConfigurer request(HttpServletRequest request);
    }

    public interface ResponseConfigurer {
        DependencyManagerConfigurer response(HttpServletResponse response);
    }

    public interface DependencyManagerConfigurer {
        RequestHandlerFactory dependencyManager(DependencyManager factory);
    }

//    public interface PropertiesConfigurer {
//        ObjectManagerConfigurer properties(TusUploadConfigurationProperties properties);
//    }
//
//    public interface ObjectManagerConfigurer {
//        RequestHandlerFactory manager(ObjectManager manager);
//    }

    class RequestHandlerConfigurerImpl implements RequestHandlerConfigurer {
        HttpServletRequest request;
        HttpServletResponse response;
        TusUploadConfigurationProperties properties;
        ObjectManager objectManager;

        @Override
        public RequestConfigurer configure() {
            return request -> {
                RequestHandlerConfigurerImpl.this.request = request;
                return (ResponseConfigurer) response -> {
                    RequestHandlerConfigurerImpl.this.response = response;
                    return (DependencyManagerConfigurer) factory -> new RequestHandlerFactory.RequestHandlerFactoryImpl(request,response,factory);
//                    return (PropertiesConfigurer) properties -> {
//                        RequestHandlerConfigurerImpl.this.properties = properties;
//                        return new ObjectManagerConfigurer() {
//                            @Override
//                            public RequestHandlerFactory manager(ObjectManager manager) {
//                                return new RequestHandlerFactory.RequestHandlerFactoryImpl(request, response, properties,manager);
//                            }
//                        };
//
//                    };
                };
            };
        }
    }
}
