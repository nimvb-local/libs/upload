package ir.nimvb.lib.upload.service.common.step;

import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;

public interface StepsBuilder {
    ChainExecutor build(DependencyManager dependencyManager);
}
