package ir.nimvb.lib.upload.service.protocol.factory;

import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.protocol.AbstractTusProtocolRequestHandler;
import ir.nimvb.lib.upload.service.protocol.core.options.OptionsRequestHandler;
import ir.nimvb.lib.upload.service.protocol.extension.creation.PostRequestHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface RequestHandlerFactory {

    AbstractTusProtocolRequestHandler build();

    @RequiredArgsConstructor
    class RequestHandlerFactoryImpl implements RequestHandlerFactory {

        private final HttpServletRequest request;
        private final HttpServletResponse response;
        private final DependencyManager dependencyManager;

        @Override
        public AbstractTusProtocolRequestHandler build() {
            HttpMethod httpMethod = HttpMethod.valueOf(request.getMethod());
            switch (httpMethod) {
                case GET:
                    break;
                case HEAD:
                    break;
                case POST:
                    return new PostRequestHandler(dependencyManager);
                case PUT:
                    break;
                case PATCH:
                    break;
                case DELETE:
                    break;
                case OPTIONS: {
                    return new OptionsRequestHandler(dependencyManager);
                }
                case TRACE:
                    break;
            }
            return null;
        }
    }
}
