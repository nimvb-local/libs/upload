package ir.nimvb.lib.upload.service.store.context.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.nimvb.lib.upload.exception.ObjectContextException;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.model.s3.S3ObjectContext;
import ir.nimvb.lib.upload.service.store.context.ObjectContextStore;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class S3Store implements ObjectContextStore {

    private final AmazonS3 s3Client;
    private final String bucket;
    private final ObjectMapper converter;
    private final Map<String,String> store = new HashMap<>();

    @Override
    public ObjectContext save(ObjectContext context) {
        Assert.notNull(context,"context is null");
        Assert.isInstanceOf(S3ObjectContext.class,context,"s3 object context expected");
        try{
            String json = converter.writeValueAsString(context);
            store.put(context.getId(), json);
        }catch (Exception ex){
            throw new ObjectContextException(ex);
        }
        return context;
    }

    @Override
    public Optional<ObjectContext> find(String id) {
        if(!store.containsKey(id)){
            return Optional.empty();
        }
        try {
            String json = store.get(id);
            return Optional.ofNullable(converter.readValue(json, S3ObjectContext.class));
        }catch (Exception ignored){
            return Optional.empty();
        }
    }

    @Override
    public Set<ObjectContext> findAll() {
        return store.values().stream().map(s -> {
            try {
                return converter.readValue(s,S3ObjectContext.class);
            } catch (JsonProcessingException ignored) {

            }
            return null;
        }).collect(Collectors.toSet());
    }

    @Override
    public void delete(String id) {
        store.remove(id);
    }

}
