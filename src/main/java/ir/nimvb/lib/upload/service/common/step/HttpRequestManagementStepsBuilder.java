package ir.nimvb.lib.upload.service.common.step;

import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface HttpRequestManagementStepsBuilder {

    ChainExecutor build(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager);

}
