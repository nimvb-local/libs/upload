package ir.nimvb.lib.upload.service.common.request;

import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NotFoundRequestHandler extends AbstractRequestHandler {
    public NotFoundRequestHandler(DependencyManager dependencyManager) {
        super(dependencyManager);
    }

    @Override
    protected HttpStatus status() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    protected void process(HttpServletRequest request, HttpServletResponse response) {
        response.setStatus(status().value());
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.GET;
    }

    @Override
    public String path() {
        return "/";
    }

    @Override
    public String[] pathVariables() {
        return new String[0];
    }
}
