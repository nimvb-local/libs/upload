package ir.nimvb.lib.upload.service.common.bean;

import ir.nimvb.lib.upload.controller.model.DispatcherContext;
import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
import ir.nimvb.lib.upload.service.manager.ObjectManager;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.util.PathMatcher;
import org.springframework.web.util.UrlPathHelper;

@Component
@Getter
public class DependencyManager {

    private final ObjectManager objectManager;
    private final TusUploadConfigurationProperties properties;
    @Getter(AccessLevel.NONE)
    private final AsyncTaskExecutor taskExecutor;
    @Lazy
    private final PathMatcher pathMatcher;
    @Lazy
    private final UrlPathHelper urlPathHelper;
    @Lazy
    private final DispatcherContext dispatcherContext;

    @Autowired
    public DependencyManager(ObjectManager objectManager, TusUploadConfigurationProperties properties, AsyncTaskExecutor taskExecutor, @Lazy PathMatcher pathMatcher, @Lazy UrlPathHelper urlPathHelper, @Lazy DispatcherContext dispatcherContext) {
        this.objectManager = objectManager;
        this.properties = properties;
        this.taskExecutor = taskExecutor;
        this.pathMatcher = pathMatcher;
        this.urlPathHelper = urlPathHelper;
        this.dispatcherContext = dispatcherContext;
    }

    public ConcurrencyManager concurrency() {
        return new ConcurrencyManager() {
            @Override
            public AsyncTaskExecutor taskExecutor() {
                return DependencyManager.this.taskExecutor;
            }
        };
    }


    public interface ConcurrencyManager {
        public AsyncTaskExecutor taskExecutor();
    }
}
