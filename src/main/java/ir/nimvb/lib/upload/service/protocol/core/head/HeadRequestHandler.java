package ir.nimvb.lib.upload.service.protocol.core.head;

import com.nimvb.lib.pattern.behavioral.chain.ChainExecutionReport;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.protocol.AbstractTusProtocolRequestHandler;
import ir.nimvb.lib.upload.service.protocol.core.head.model.RequestContext;
import ir.nimvb.lib.upload.service.protocol.core.head.step.builder.HeadStepBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerMapping;

import java.util.Collections;
import java.util.Map;

public class HeadRequestHandler extends AbstractTusProtocolRequestHandler {

    private HttpStatus status = HttpStatus.NOT_FOUND;

    public HeadRequestHandler(DependencyManager dependencyManager) {
        super(dependencyManager);
    }

    @Override
    protected void process() {
        StageContext context = new StageContext();
        RequestContext content = new RequestContext();
        content.setObjectId(getId());
        context.setContent(content);
        ChainExecutionReport report = HeadStepBuilder.INSTANCE.build(request, response, dependencyManager).execute(context);
        status = ((RequestContext) report.content()).getStatus();
        response.setHeader(HttpHeaders.CACHE_CONTROL,"no-store");
    }

    public String getId() {
        return ((Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE)).get("id");
    }

    @Override
    protected HttpStatus status() {
        return status;
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.HEAD;
    }

    @Override
    public String path() {
        return "/{id}";
    }

    @Override
    public String[] pathVariables() {
        return Collections.singletonList("id").toArray(new String[0]);
    }
}
