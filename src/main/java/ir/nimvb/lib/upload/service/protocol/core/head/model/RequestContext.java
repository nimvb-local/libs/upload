package ir.nimvb.lib.upload.service.protocol.core.head.model;

import ir.nimvb.lib.upload.model.ObjectContext;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Optional;

@Getter
public class RequestContext {

    @Setter
    private HttpStatus status = HttpStatus.NOT_FOUND;
    @Setter
    private String objectId;
    @Setter
    private Optional<ObjectContext> objectContext;
}
