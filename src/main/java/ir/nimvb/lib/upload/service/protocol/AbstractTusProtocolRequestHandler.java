package ir.nimvb.lib.upload.service.protocol;

import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.request.AbstractRequestHandler;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public abstract class AbstractTusProtocolRequestHandler extends AbstractRequestHandler {

    protected HttpServletRequest request;
    protected HttpServletResponse response;

    public AbstractTusProtocolRequestHandler(DependencyManager dependencyManager) {
        super(dependencyManager);
    }
//    protected HttpServletRequest request;
//    protected HttpServletResponse response;
//    protected final DependencyManager dependencyManager;


    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        if (handleTusResumable()) {
            process();
            this.response.setHeader(TusHttpHeaders.TUS_RESUMABLE, ProtocolConstants.VERSION);
            this.response.setStatus(status().value());
        }

    }

    protected abstract void process();

    protected boolean handleTusResumable() {
        String value = request.getHeader(TusHttpHeaders.TUS_RESUMABLE);
        if (value != null) {
            if (value.strip().equals(ProtocolConstants.VERSION)) {
                return true;
            }
        }

        response.setStatus(HttpStatus.PRECONDITION_FAILED.value());

        return false;
    }

//    protected boolean isHeaderExists(String header) {
//        String value = request.getHeader(header);
//        if (value == null) {
//            return false;
//        }
//        return true;
//    }
}
