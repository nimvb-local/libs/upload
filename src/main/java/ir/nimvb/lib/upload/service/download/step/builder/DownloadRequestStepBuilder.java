package ir.nimvb.lib.upload.service.download.step.builder;

import com.nimvb.lib.pattern.behavioral.chain.Chain;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.builder.ExecutorBuilder;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.HttpRequestManagementStepsBuilder;
import ir.nimvb.lib.upload.service.download.step.manager.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadRequestStepBuilder implements HttpRequestManagementStepsBuilder {
    public static final DownloadRequestStepBuilder INSTANCE = new DownloadRequestStepBuilder();
    private DownloadRequestStepBuilder(){}
    @Override
    public ChainExecutor build(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        ExecutorBuilder builder = Chain.INSTANCE.builder();
        builder.add(new ConcurrentResultCheckingStep(request,response,dependencyManager));
        builder.add(new ObjectIdValidationStep(request,response,dependencyManager));
        builder.add(new ContentTypeEvaluationStep(request,response,dependencyManager));
        builder.add(new ContentDispositionEvaluationStep(request,response,dependencyManager));
        builder.add(new ResponseBodyBuildingStep(request,response,dependencyManager));
        return builder.build();
    }
}
