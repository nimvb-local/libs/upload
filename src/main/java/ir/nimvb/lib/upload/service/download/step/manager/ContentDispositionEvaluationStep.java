package ir.nimvb.lib.upload.service.download.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.download.model.DownloadRequestContext;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class ContentDispositionEvaluationStep extends AbstractStepManagerWithContentType<DownloadRequestContext> {
    public ContentDispositionEvaluationStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(DownloadRequestContext.class, request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        DownloadRequestContext content = content(context);
        ObjectContext objectContext = content.getObjectContext().get();
        Optional<String> value = objectContext.getMetadata().value("filename");
        value.ifPresent(s -> response.setHeader(HttpHeaders.CONTENT_DISPOSITION, ContentDisposition.attachment().filename(s).build().toString()));
        chain.next(context);
        return context;
    }
}
