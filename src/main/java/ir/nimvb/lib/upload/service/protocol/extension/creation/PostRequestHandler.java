package ir.nimvb.lib.upload.service.protocol.extension.creation;

import com.nimvb.lib.pattern.behavioral.chain.ChainExecutionReport;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.util.Request;
import ir.nimvb.lib.upload.service.protocol.AbstractTusProtocolRequestHandler;
import ir.nimvb.lib.upload.service.protocol.extension.Extension;
import ir.nimvb.lib.upload.service.protocol.extension.creation.model.CreationExtensionContext;
import ir.nimvb.lib.upload.service.protocol.extension.creation.step.builder.ExtensionStepBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

public class PostRequestHandler extends AbstractTusProtocolRequestHandler implements Extension {

    private HttpStatus status = HttpStatus.BAD_REQUEST;

    public PostRequestHandler(DependencyManager dependencyManager) {
        super(dependencyManager);
    }


    @Override
    protected void process() {

        if (!Request.of(request).headers().exists(TusHttpHeaders.UPLOAD_LENGTH) && !Request.of(request).headers().exists(TusHttpHeaders.UPLOAD_DEFER_LENGTH)) {
            status = HttpStatus.BAD_REQUEST;
            return;
        }

        StageContext context = new StageContext();
        context.setContent(new CreationExtensionContext());
        ChainExecutionReport report = ExtensionStepBuilder.INSTANCE.build(request, response, dependencyManager).execute(context);
        CreationExtensionContext content = report.content();
        status = content.getStatus();
//        if (content != null) {
//            if (content.getStatus() == HttpStatus.CREATED) {
//
//            }
//
//        }
    }

    @Override
    protected HttpStatus status() {
        return status;
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.POST;
    }

    @Override
    public String path() {
        return "";
    }

    @Override
    public String[] pathVariables() {
        return new String[0];
    }

    @Override
    public String name() {
        return "creation";
    }
}
