package ir.nimvb.lib.upload.service.protocol.core.options.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.protocol.core.options.model.OptionsRequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TusMaxUploadSizeScanningStep extends AbstractStepManagerWithContentType<OptionsRequestContext> {
    public TusMaxUploadSizeScanningStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(OptionsRequestContext.class, request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        if (dependencyManager.getProperties().getRequest().getSize().getMaximum() > 0) {
            response.setHeader(TusHttpHeaders.TUS_MAX_SIZE, String.valueOf(dependencyManager.getProperties().getRequest().getSize().getMaximum()));
        }
        chain.next(context);
        return context;
    }
}
