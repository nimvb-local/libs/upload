package ir.nimvb.lib.upload.service.protocol.core.head.step.builder;

import com.nimvb.lib.pattern.behavioral.chain.Chain;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.builder.ExecutorBuilder;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.HttpRequestManagementStepsBuilder;
import ir.nimvb.lib.upload.service.protocol.core.head.step.manager.ObjectIdValidationStep;
import ir.nimvb.lib.upload.service.protocol.core.head.step.manager.UploadLengthScanningStep;
import ir.nimvb.lib.upload.service.protocol.core.head.step.manager.UploadMetadataScanningStep;
import ir.nimvb.lib.upload.service.protocol.core.head.step.manager.UploadOffsetScanningStep;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HeadStepBuilder implements HttpRequestManagementStepsBuilder {
    public static final HttpRequestManagementStepsBuilder INSTANCE = new HeadStepBuilder();

    private HeadStepBuilder() {
    }

    @Override
    public ChainExecutor build(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        ExecutorBuilder builder = Chain.INSTANCE.builder();
        builder.add(new ObjectIdValidationStep(request,response, dependencyManager));
        builder.add(new UploadOffsetScanningStep(request,response, dependencyManager));
        builder.add(new UploadLengthScanningStep(request,response, dependencyManager));
        builder.add(new UploadMetadataScanningStep(request,response, dependencyManager));
        return builder.build();
    }
}
