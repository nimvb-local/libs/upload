package ir.nimvb.lib.upload.service.store;

import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.property.TusUploadConfigurationProperties;
import ir.nimvb.lib.upload.service.store.result.CreateResult;
import ir.nimvb.lib.upload.service.store.result.WriteResult;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Manages the storage objects.
 * Operations on storage objects may require to modify object contexts, so the requests to the operations are all immutable
 * and the result object contains the final object context version
 *
 * @see ir.nimvb.lib.upload.service.store.result.Result
 */
public interface DataStore {

    /**
     * Creates a new storage object
     * @param context
     * @return the new usable context
     */
    CreateResult create(ObjectContext context);

    /**
     * Delete the storage object related to the context object given with {@code context} parameter
     * @param context
     */
    void delete(ObjectContext context);

    /**
     * Determines the current offset of the storage object
     * @param context The context of the desired storage object
     * @return
     */
    long length(ObjectContext context);

    ObjectContext finish(ObjectContext context);

    interface Writer {
        WriteResult write(ObjectContext context, InputStream stream);
    }

    interface Reader {
        byte[] read(String id, long offset, long length);
        InputStream getInputStream(String id) throws IOException;
    }

    public interface Util {
        public interface Path {
            public static String store(TusUploadConfigurationProperties properties, String id){
                return properties.getFolder() + File.separator + id + ".bin";
            }
            public static String deleted(TusUploadConfigurationProperties properties, String id){
                return properties.getFolder() + File.separator + id + ".bin" + ".deleted";
            }
        }
    }
}
