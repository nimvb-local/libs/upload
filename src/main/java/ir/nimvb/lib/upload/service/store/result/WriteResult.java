package ir.nimvb.lib.upload.service.store.result;

import ir.nimvb.lib.upload.model.ObjectContext;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper = true)

@Getter
public class WriteResult extends Result {
    private final long count;

    public WriteResult(ObjectContext context, long count) {
        super(context);
        this.count = count;
    }
}
