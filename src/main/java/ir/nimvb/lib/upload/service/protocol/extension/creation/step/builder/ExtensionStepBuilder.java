package ir.nimvb.lib.upload.service.protocol.extension.creation.step.builder;

import com.nimvb.lib.pattern.behavioral.chain.Chain;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.builder.ExecutorBuilder;
import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.util.Request;
import ir.nimvb.lib.upload.service.common.step.HttpRequestManagementStepsBuilder;
import ir.nimvb.lib.upload.service.protocol.extension.creation.step.manager.DeferLengthManagementStep;
import ir.nimvb.lib.upload.service.protocol.extension.creation.step.manager.UploadLengthManagementStep;
import ir.nimvb.lib.upload.service.protocol.extension.creation.step.manager.UploadMetadataManagementStep;
import ir.nimvb.lib.upload.service.protocol.extension.creation.step.manager.ObjectContextPersistenceManagementStep;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ExtensionStepBuilder implements HttpRequestManagementStepsBuilder {

    private ExtensionStepBuilder() {}

    public static final ExtensionStepBuilder INSTANCE = new ExtensionStepBuilder();

    @Override
    public ChainExecutor build(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        ExecutorBuilder builder = Chain.INSTANCE.builder();
        if(Request.of(request).headers().exists(TusHttpHeaders.UPLOAD_LENGTH)){
            builder.add(new UploadLengthManagementStep(request,response,dependencyManager));
        }else if(Request.of(request).headers().exists(TusHttpHeaders.UPLOAD_DEFER_LENGTH)) {
            builder.add(new DeferLengthManagementStep(request,response, dependencyManager));
        }
        builder.add(new UploadMetadataManagementStep(request,response, dependencyManager));
        builder.add(new ObjectContextPersistenceManagementStep(request,response, dependencyManager));
        return builder.build();
    }
}
