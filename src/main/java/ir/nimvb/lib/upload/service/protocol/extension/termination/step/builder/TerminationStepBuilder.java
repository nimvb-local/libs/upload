package ir.nimvb.lib.upload.service.protocol.extension.termination.step.builder;

import com.nimvb.lib.pattern.behavioral.chain.Chain;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.builder.ExecutorBuilder;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.HttpRequestManagementStepsBuilder;
import ir.nimvb.lib.upload.service.protocol.extension.termination.step.manager.ObjectIdValidationStep;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TerminationStepBuilder implements HttpRequestManagementStepsBuilder {

    public static final TerminationStepBuilder INSTANCE = new TerminationStepBuilder();

    private TerminationStepBuilder(){}

    @Override
    public ChainExecutor build(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        ExecutorBuilder builder = Chain.INSTANCE.builder();
        builder.add(new ObjectIdValidationStep(request,response,dependencyManager));
        return builder.build();
    }
}
