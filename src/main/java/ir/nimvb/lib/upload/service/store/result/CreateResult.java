package ir.nimvb.lib.upload.service.store.result;

import ir.nimvb.lib.upload.model.ObjectContext;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public final class CreateResult extends Result {

    public CreateResult(ObjectContext context) {
        super(context);
    }
}
