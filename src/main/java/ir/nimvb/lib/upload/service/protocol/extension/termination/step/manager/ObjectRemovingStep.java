package ir.nimvb.lib.upload.service.protocol.extension.termination.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.protocol.extension.termination.model.TerminationExtensionContext;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ObjectRemovingStep extends AbstractStepManagerWithContentType<TerminationExtensionContext> {
    public ObjectRemovingStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(TerminationExtensionContext.class, request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        TerminationExtensionContext content = content(context);
        dependencyManager.getObjectManager().delete(content.getObjectId());
        content.setStatus(HttpStatus.NO_CONTENT);
        chain.next(context);
        return context;
    }
}
