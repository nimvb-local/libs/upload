package ir.nimvb.lib.upload.service.protocol.core.options;

import com.nimvb.lib.pattern.behavioral.chain.ChainExecutionReport;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.protocol.AbstractTusProtocolRequestHandler;
import ir.nimvb.lib.upload.service.protocol.core.options.model.OptionsRequestContext;
import ir.nimvb.lib.upload.service.protocol.core.options.step.builder.OptionsStepBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

public class OptionsRequestHandler extends AbstractTusProtocolRequestHandler {


    private HttpStatus status = HttpStatus.NO_CONTENT;

    public OptionsRequestHandler(DependencyManager dependencyManager) {
        super(dependencyManager);
    }

    @Override
    protected void process() {
        StageContext context = new StageContext();
        OptionsRequestContext optionsRequestContext = new OptionsRequestContext();
        context.setContent(optionsRequestContext);
        ChainExecutionReport report = OptionsStepBuilder.INSTANCE.build(request, response, dependencyManager).execute(context);
        status = ((OptionsRequestContext) report.content()).getStatus();

//        response.setHeader(TusHttpHeaders.TUS_VERSION, ProtocolConstants.VERSION);
//        response.setHeader(TusHttpHeaders.TUS_EXTENSION, ProtocolConstants.EXTENSIONS);
//        if (dependencyManager.getProperties().getRequest().getSize().getMaximum() > 0) {
//            response.setHeader(TusHttpHeaders.TUS_MAX_SIZE, String.valueOf(dependencyManager.getProperties().getRequest().getSize().getMaximum()));
//        }
    }

    @Override
    protected HttpStatus status() {
        return status;
    }

    @Override
    protected boolean handleTusResumable() {
        return true;
    }

    @Override
    public HttpMethod method() {
        return HttpMethod.OPTIONS;
    }

    @Override
    public String path() {
        return "/**";
    }

    @Override
    public String[] pathVariables() {
        return new String[0];
    }
}
