package ir.nimvb.lib.upload.service.download.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.download.model.DownloadRequestContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.async.WebAsyncManager;
import org.springframework.web.context.request.async.WebAsyncUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ConcurrentResultCheckingStep extends AbstractStepManagerWithContentType<DownloadRequestContext> {
    public ConcurrentResultCheckingStep( HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(DownloadRequestContext.class, request, response, dependencyManager);
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        DownloadRequestContext content = content(context);
        WebAsyncManager asyncManager = WebAsyncUtils.getAsyncManager(request);
        if(asyncManager.hasConcurrentResult()){
            content.setStatus(HttpStatus.OK);
        }else {
            chain.next(context);
        }
        return context;
    }
}
