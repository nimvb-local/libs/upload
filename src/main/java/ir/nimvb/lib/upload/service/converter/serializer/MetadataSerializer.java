package ir.nimvb.lib.upload.service.converter.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ir.nimvb.lib.upload.model.Metadata;

import java.io.IOException;

public class MetadataSerializer extends StdSerializer<Metadata> {

    public MetadataSerializer() {
        this(Metadata.class);
    }

    protected MetadataSerializer(Class<Metadata> t) {
        super(t);
    }

    @Override
    public void serialize(Metadata value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("value",value.encode());
        gen.writeEndObject();
    }
}
