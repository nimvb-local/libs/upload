package ir.nimvb.lib.upload.service.download.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.download.model.DownloadRequestContext;
import ir.nimvb.lib.upload.service.protocol.common.step.manager.AbstractObjectIdValidationStep;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class ObjectIdValidationStep extends AbstractObjectIdValidationStep<DownloadRequestContext> {
    public ObjectIdValidationStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(DownloadRequestContext.class, request, response, dependencyManager);
    }

    @Override
    protected String getObjectId(StageContext context) {
        DownloadRequestContext content = content(context);
        return content.getObjectId();
    }

    @Override
    protected void setObjectContext(StageContext context, Optional<ObjectContext> objectContext) {
        DownloadRequestContext content = content(context);
        content.setObjectContext(objectContext);
    }

    @Override
    protected void setStatus(StageContext context, HttpStatus status) {
        DownloadRequestContext content = content(context);
        content.setStatus(status);
    }
}
