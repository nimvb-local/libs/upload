package ir.nimvb.lib.upload.service.protocol.core.head.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.model.protocol.tus.http.TusHttpHeaders;
import ir.nimvb.lib.upload.model.ObjectContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManager;
import ir.nimvb.lib.upload.service.protocol.core.head.model.RequestContext;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UploadLengthScanningStep extends AbstractStepManager {
    public UploadLengthScanningStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(request, response, dependencyManager);
    }

    @Override
    protected <T> Class<T> contentType() {
        return (Class<T>) RequestContext.class;
    }

    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        RequestContext requestContext = (RequestContext) context.getContent();
        Assert.isTrue(requestContext.getObjectContext().isPresent(),"invalid object context");
        ObjectContext objectContext = requestContext.getObjectContext().get();
        if(objectContext.getLength() > 0){
            response.setHeader(TusHttpHeaders.UPLOAD_LENGTH,String.valueOf(objectContext.getLength()));
        }
        chain.next(context);
        return context;
    }
}
