package ir.nimvb.lib.upload.service.download.model;

import ir.nimvb.lib.upload.model.ObjectContext;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Optional;

@RequiredArgsConstructor
@Getter
public class DownloadRequestContext {
    private final String objectId;
    @Setter
    private HttpStatus status;
    @Setter
    Optional<ObjectContext> objectContext = Optional.empty();

}
