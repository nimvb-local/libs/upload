package ir.nimvb.lib.upload.service.protocol.extension.creation.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.Stage;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import ir.nimvb.lib.upload.service.common.step.manager.AbstractStepManagerWithContentType;
import ir.nimvb.lib.upload.service.protocol.extension.creation.model.CreationExtensionContext;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeferLengthManagementStep extends AbstractStepManagerWithContentType<CreationExtensionContext> implements Stage {


    public DeferLengthManagementStep(HttpServletRequest request, HttpServletResponse response, DependencyManager dependencyManager) {
        super(CreationExtensionContext.class, request, response, dependencyManager);
    }



    @Override
    protected StageContext run(StageContext context, LayerExecutor chain) {
        CreationExtensionContext content = context.getContent();
        content.setStatus(HttpStatus.BAD_REQUEST);
        return context;
    }
}
