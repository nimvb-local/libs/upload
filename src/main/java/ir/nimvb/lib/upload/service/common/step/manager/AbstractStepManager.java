package ir.nimvb.lib.upload.service.common.step.manager;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.Stage;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import ir.nimvb.lib.upload.service.common.bean.DependencyManager;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
public abstract class AbstractStepManager implements Stage {
    protected final HttpServletRequest request;
    protected final HttpServletResponse response;
    protected final DependencyManager dependencyManager;

    @Override
    public StageContext execute(StageContext context, LayerExecutor chain) {
        asserts(context);
        return run(context, chain);
    }

    protected abstract <T> Class<T> contentType();

    protected abstract StageContext run(StageContext context, LayerExecutor chain);

    protected void asserts(StageContext context) {
        Assert.notNull(context, "context is null");
        Assert.notNull(context.getContent(), "content is null");
        Assert.isInstanceOf(contentType(), context.getContent(), "invalid content type");
    }
}
